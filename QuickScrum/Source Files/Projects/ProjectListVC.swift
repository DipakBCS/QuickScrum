//
//  ProjectListVC.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 20/02/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire

class ProjectListVC :  UIViewController  {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var menuView : CommonView!
    var selectProjectView : SearchTableView!
    
    
    var authorizationHeader: HTTPHeaders {
        let str = String(format:"Basic %@",UserDefaults.standard.value(forKey: TOKEN) as! CVarArg)
        if !str.isEmpty { //  if let token = str.data(using: .utf8) {
            return ["Authorization": str]
        }
        return ["Authorization": ""]
    }
    
    var queue = OperationQueue()
    
    //MARK:
    //MARK: View Life Cycle
    //MARK:
    
    override func viewDidLoad() {
        super.viewDidLoad()
        height = (self.navigationController?.navigationBar.bounds.size.height)! + 20
        //configureView()
        self.title = "Select Project"
        configureProjectSelectionView()
        multipleAPICallTogether()
       // let swiftRight = UISwipeGestureRecognizer(target: self, action: #selector(swiftRightClicked(gesture:)))
       // swiftRight.direction = .right
       // self.view.addGestureRecognizer(swiftRight)
    }
    
    func multipleAPICallTogether(){
        let que = BlockOperation(block: {
            // self.startLoading()
            self.getProjects()
        })
        que.completionBlock = {
            if (self.queue.operationCount == 0){
             //   self.hideLoading()
                print("All operatation are completed.")
            }
        }
        self.queue.addOperation(que)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    //MARK:Reuseble method
    //MARK:
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.hidesBottomBarWhenPushed = true
    }
    
    func configureProjectSelectionView(){
        if let searchView = Bundle.main.loadNibNamed("SearchTableView", owner: self, options: nil)?.first  as? SearchTableView {
            searchView.frame = CGRect(x: 0, y: height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - height)
            searchView.backgroundColor  = LGC
            searchView.parentVC = self 
            self.selectProjectView = searchView
            self.view.addSubview(searchView)
        }
    }
    //
    //MARK:
    //MARK: Action Event Method
    @IBAction func btnProjectSelectetionClicked(){
        if self.selectProjectView != nil {
            let hei = self.selectProjectView.bounds.height
            if hei == 0 {
                showSearchTableView()
            }
            else {
                hideSearchTableView()
            }
        }
    }
    @IBAction func btnMenuClicked(){
        self.menuView.isHidden = false
        showView()
    }
    
    //MARK:
    //MARK: Reiseble Useful Method
    
    
    func showView(){
        hideSearchTableView()
        if menuView != nil {
            self.menuView.fadeIn()
        }
    }
    func hideMenuView(){
        if menuView != nil {
            self.menuView.fadeOut()
        }
    }
    func showSearchTableView(){
        hideMenuView()
        if self.selectProjectView != nil {
            selectProjectView.frame = CGRect(x: 0, y: height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - height)
        }
    }
    func hideSearchTableView(){
        if self.selectProjectView != nil {
            selectProjectView.frame = CGRect(x: 0, y: height, width: UIScreen.main.bounds.width, height: 0)
        }
    }
    func resetConstraint(inView:UIView,parentView:UIView){
        inView.leadingAnchor.constraint(equalTo: (parentView.leadingAnchor), constant:0).isActive = true
        inView.trailingAnchor.constraint(equalTo: (parentView.trailingAnchor), constant: 0).isActive = true
        parentView.updateConstraints()
        parentView.layoutIfNeeded()
    }
    
    // To reuseble code for to set up a view
    func setView(parentView:UIView,myView:UIView,leadX:CGFloat,trailY:CGFloat,top:CGFloat,bottm:CGFloat){
        parentView.addSubview(myView)
        myView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setConstraint(parentView:UIView,myView:UIView,leadX:CGFloat,trailY:CGFloat,top:CGFloat,bottm:CGFloat){
        myView.leadingAnchor.constraint(equalTo: (parentView.leadingAnchor), constant: leadX).isActive = true
        myView.trailingAnchor.constraint(equalTo: (parentView.trailingAnchor), constant: trailY).isActive = true
        myView.topAnchor.constraint(equalTo: (parentView.topAnchor), constant: top).isActive = true
        myView.bottomAnchor.constraint(equalTo: (parentView.bottomAnchor), constant: bottm).isActive = true
    }
    
    //MARK:
    //MARK: Server Communication Method
    //MARK:
    
    
    func getProjects(){
        if GlobalClass.sharedInstance.checkInternet().boolValue{
            let token = UserDefaults.standard.value(forKey: TOKEN) as? String
            if token != nil , !(token?.isEmpty)! {
                self.startLoading()
                let url = String(format: "%@/api/v1/getProject", BASE_URL)
                Alamofire.request(url, method: .post, parameters:nil,encoding: JSONEncoding.default, headers: self.authorizationHeader).responseJSON {
                    response in
                    self.hideLoading()
                    print("getProject completed")
                  //  print(response.result.value)
                    switch response.result {
                    case .success:
                        if let arr1 = response.result.value as? [String:Any]  {
                            if let arr = arr1["resultData"] as? [[String:Any]], arr.count > 0 {
                                var projectsArr = [Projects]()
                                for dic in arr {
                                    let project = Projects(json: dic)
                                    projectsArr.append(project)
                                }
                                if self.selectProjectView != nil {
                                    self.selectProjectView.projectArr = projectsArr
                                    if self.selectProjectView.tblProject  != nil {
                                        self.selectProjectView.tblProject.reloadData()
                                    }
                                }
                            }
                        }
                        break
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }
        else {
            self.showAlert(message: CHECK_INTERNET)
        }
    }
    
    
    @IBAction func cancelClicked(){
        self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}



