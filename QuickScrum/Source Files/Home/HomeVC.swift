//
//  HomeVC.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 04/02/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
// git push origin HEAD:Version1

import UIKit
import Alamofire


class HomeVC: UIViewController , UITableViewDataSource,UITableViewDelegate {
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var menuView : CommonView!
    var selectProjectView : SearchTableView!
    var user : User!    
    @IBOutlet weak var tblNotification : UITableView!
    
    var arrNotification : [Notification]!
    var authorizationHeader: HTTPHeaders {
        let str = String(format:"Basic %@",UserDefaults.standard.value(forKey: TOKEN) as! CVarArg)
                if !str.isEmpty { //  if let token = str.data(using: .utf8) {
            return ["Authorization": str]
        }
        return ["Authorization": ""]
    }
    var sectionArr = ["Today","Last Week","Last Month"]
    
    var queue = OperationQueue()
    let gradientLayer = CAGradientLayer()    
    //MARK:
    //MARK: View Life Cycle
    //MARK:
    override func viewDidLoad() {
        super.viewDidLoad()
        height = (self.navigationController?.navigationBar.bounds.size.height)! + 20
        if tblNotification != nil {
            self.tblNotification.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
            self.tblNotification.separatorStyle = .none
            
        }
        configureView()
      // configureProjectSelectionView()
        multipleAPICallTogether()
        let swiftRight = UISwipeGestureRecognizer(target: self, action: #selector(swiftRightClicked(gesture:)))
        swiftRight.direction = .right
        self.view.addGestureRecognizer(swiftRight)
    }
    
    func multipleAPICallTogether(){
        let que = BlockOperation(block: {
           // self.startLoading()
            self.getUserDetail()
            self.getUserNotification()
          //  self.getProjects()
        })
        que.completionBlock = {
            if (self.queue.operationCount == 0){
                self.hideLoading()
                print("All operatation are completed.")
            }
        }
        self.queue.addOperation(que)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if tblNotification != nil {
            let indexPath = tblNotification.indexPathForSelectedRow
            if indexPath != nil {
                tblNotification.deselectRow(at: indexPath!, animated: false)
            }
        }
       // btnMenuClicked()
    }
    //MARK:Reuseble method
    //MARK:
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.hidesBottomBarWhenPushed = true
    }
    func configureView(){        
        if let menuView = Bundle.main.loadNibNamed("CommonView", owner: self, options: nil)?.first  as? CommonView {
           // menuView.backgroundColor = CC
            menuView.frame = (appDelegate.window?.frame)!
            self.menuView = menuView
            
            self.menuView.backgroundColor = LGC.withAlphaComponent(0.3)
            self.menuView.fadeOut()
            appDelegate.window?.addSubview(menuView)
        }
    }
    func configureProjectSelectionView(){
        if let searchView = Bundle.main.loadNibNamed("SearchTableView", owner: self, options: nil)?.first  as? SearchTableView {
            searchView.frame = CGRect(x: 0, y: height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - height)
            //searchView.backgroundColor  = LGC
            self.selectProjectView = searchView
            self.view.addSubview(searchView)
        }
    }
    //MARK:
    //MARK: Action Event Method
    @IBAction func btnProjectSelectetionClicked(){
        if self.selectProjectView != nil {
            let hei = self.selectProjectView.bounds.height
            if hei == 0 {
                showSearchTableView()
            }
            else {
                hideSearchTableView()
            }
        }
    }
    @IBAction func btnMenuClicked(){
        self.menuView.isHidden = false
        showView()
    }
    @IBAction func btnProfileClicked(){
        hideMenuView()
        hideSearchTableView()
        if let objVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC")  as? ProfileVC {
            if user != nil {
                objVC.user = user
            }
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    //MARK:
    //MARK: Reiseble Useful Method
    
    func swiftRightClicked(gesture:UISwipeGestureRecognizer){
        if gesture.direction == .right {
            showView()
        }
    }
    func showView(){
        hideSearchTableView()
        if menuView != nil {
            self.menuView.fadeIn()
        }
    }
    func hideMenuView(){
        if menuView != nil {
            self.menuView.fadeOut()
        }
    }
    func showSearchTableView(){
        hideMenuView()
         if self.selectProjectView != nil {
             selectProjectView.frame = CGRect(x: 0, y: height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - height)
        }
    }    
    func hideSearchTableView(){
        if self.selectProjectView != nil {
             selectProjectView.frame = CGRect(x: 0, y: height, width: UIScreen.main.bounds.width, height: 0)
        }
    }
    func resetConstraint(inView:UIView,parentView:UIView){
        inView.leadingAnchor.constraint(equalTo: (parentView.leadingAnchor), constant:0).isActive = true
        inView.trailingAnchor.constraint(equalTo: (parentView.trailingAnchor), constant: 0).isActive = true
        parentView.updateConstraints()
        parentView.layoutIfNeeded()
    }
    
   // To reuseble code for to set up a view
   func setView(parentView:UIView,myView:UIView,leadX:CGFloat,trailY:CGFloat,top:CGFloat,bottm:CGFloat){
        parentView.addSubview(myView)
        myView.translatesAutoresizingMaskIntoConstraints = false    
    }
    
    func setConstraint(parentView:UIView,myView:UIView,leadX:CGFloat,trailY:CGFloat,top:CGFloat,bottm:CGFloat){
        myView.leadingAnchor.constraint(equalTo: (parentView.leadingAnchor), constant: leadX).isActive = true
        myView.trailingAnchor.constraint(equalTo: (parentView.trailingAnchor), constant: trailY).isActive = true
        myView.topAnchor.constraint(equalTo: (parentView.topAnchor), constant: top).isActive = true
        myView.bottomAnchor.constraint(equalTo: (parentView.bottomAnchor), constant: bottm).isActive = true
    }
    
    
    //MARK:
    //MARK: UITableView METHOD
    //MARK:
   
    func reloadTable(){
        if tblNotification != nil  {
            DispatchQueue.main.async(execute: {
                self.tblNotification.reloadData()
            })
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrNotification != nil {
            return arrNotification.count
        }
        return 0
    }
    /*func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let tit = sectionArr[section]
        let arr  = arrNotification.filter({( notification : Notification) -> Bool in
            return notification.sectionTitle.lowercased().contains(tit.lowercased())
        })
        if arr != nil {
            return arr.count
        }        
        return 0
    }*/
    
   /* func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArr.count
    }*/
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20//UITableViewAutomaticDimension
    }
   /* func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionArr[section]
    }*/
    
    /*func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ProfileView") as? ProfileView{
            guard let usrObj = self.user else {
                print("UserObject is nil")
                return UIView()
        }
            
            headerView.profileImage.image = #imageLiteral(resourceName: "profileimg_men")
            headerView.lblName.text = usrObj.fullName
            headerView.lblEmail.text = usrObj.loginEmailId
            return headerView
        }
        return UIView()
    }*/
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let  cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as? NotificationCell {
            if arrNotification.count > indexPath.row {
                let objNoti = arrNotification[indexPath.row]
                if objNoti.comment != nil {
                    cell.lblTitle.text = objNoti.comment
                    cell.lblAssignee.text = String(format:"Created By %@",objNoti.createdByUser.fullName!)
                    cell.lblTaskId.text = String(format:"%d",objNoti.id)
                    cell.lblCreatedDate.text = objNoti.createdDate
                    return cell
                }
                
            }
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let objVC = self.storyboard?.instantiateViewController(withIdentifier: "CommentVC") as? CommentVC {
            objVC.notification = arrNotification[indexPath.row]
            self.navigationController?.pushViewController(objVC, animated: true)
        }
         
    }
 
    
    //MARK:
    //MARK: Server Communication Method
    //MARK:
    
    func getUserNotification(){
        if GlobalClass.sharedInstance.checkInternet().boolValue{
            let token = UserDefaults.standard.value(forKey: TOKEN) as? String
            if token != nil,!(token?.isEmpty)! {
                // self.startLoading()
                let url = String(format: "%@/api/v1/getNotification", BASE_URL)
                Alamofire.request(url, method: .post, parameters:nil,encoding: JSONEncoding.default, headers: self.authorizationHeader).responseJSON {
                    response in
                    //self.hideLoading()
                   // print(response.result.value)
                    switch response.result {
                    case .success:
                        guard let dic =  response.result.value as? [String:Any] else {
                            print("responce is not Array")
                            return
                        }
                        if self.arrNotification == nil {
                            self.arrNotification = [Notification]()
                        }
                        self.arrNotification.removeAll()
                        
                        if let arr = dic["resultData"] as? [[String:Any]]{
                            for dic in arr {
                                self.arrNotification.append(Notification(json: dic))
                            }
                        }
                        self.reloadTable()
                        break
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }
        else {
            self.showAlert(message: CHECK_INTERNET)
        }
    }
    func getUserDetail(){
        if GlobalClass.sharedInstance.checkInternet().boolValue{            
            let token = UserDefaults.standard.value(forKey: TOKEN) as? String
                if token != nil,!(token?.isEmpty)! {
                   // self.startLoading()
                    let url = String(format: "%@/paymentapi/v1/UserDetail", BASE_URL)
                    Alamofire.request(url, method: .get, parameters:nil,encoding: JSONEncoding.default, headers: self.authorizationHeader).responseJSON {
                        response in
                     //    self.hideLoading()
                         //print(response.result.value)
                        switch response.result {
                        case .success:
                            guard let dic =  response.result.value as? [String:Any]else {
                                print("responce is not dictionary")
                                return
                            }
                            if let userDic = dic["user"] as? [String:Any]{
                                self.user = User(json:userDic)
                                if self.menuView != nil {                                    
                                    self.menuView.user = self.user
                                    self.menuView.reloadTable()
                                }
                            }
                            break
                        case .failure(let error):
                            print(error)
                        }
                    }
                }
        }
        else {
            self.showAlert(message: CHECK_INTERNET)
        }
    }
    
    func getProjects(){
        if GlobalClass.sharedInstance.checkInternet().boolValue{
            let token = UserDefaults.standard.value(forKey: TOKEN) as? String
            if token != nil , !(token?.isEmpty)! {
                self.startLoading()
                let url = String(format: "%@/api/v1/getProject", BASE_URL)
                Alamofire.request(url, method: .post, parameters:nil,encoding: JSONEncoding.default, headers: self.authorizationHeader).responseJSON {
                    response in
                    self.hideLoading()
                    print("getProject completed")
                    //print(response.result.value)
                    switch response.result {
                    case .success:                        
                        if let arr1 = response.result.value as? [String:Any]  {
                            if let arr = arr1["resultData"] as? [[String:Any]], arr.count > 0 {
                            var projectsArr = [Projects]()
                            for dic in arr {
                                let project = Projects(json: dic)
                                projectsArr.append(project)
                            }
                            if self.selectProjectView != nil {
                                self.selectProjectView.projectArr = projectsArr
                                if self.selectProjectView.tblProject  != nil {
                                    self.selectProjectView.tblProject.reloadData()
                                }
                            }
                            }
                        }
                        break
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }
        else {
            self.showAlert(message: CHECK_INTERNET)
        }
    }

    
    @IBAction func cancelClicked(){
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
class Notification{
    
    var comment : String!
    var projectName : String!
    var isRead : Bool!
    var id : Int!
    var itemId : Int!
    var createdByUser : User!
    var sectionTitle = ""
    var createdDate : String!
    
    init(json:[String:Any]){
     
        if let val = json["comment"] as? String{
            self.comment = val
        }
        if let val = json["projectName"] as? String{
            self.projectName = val
        }
        if let val = json["isRead"] as? Bool{
            self.isRead = val
        }
        if let val = json["id"] as? Int{
            self.id = val
        }
        if let val = json["itemId"] as? Int{
            self.itemId = val
        }        
        if let val = json["createdDate"] as? String{
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "dd MMM,yyyy"
            let date = dateFormat.date(from: val)
            if date != nil {
                self.createdDate = val 
                let calendar = Calendar.current
                
                let date1 = calendar.startOfDay(for: date!)
                let date2 = calendar.startOfDay(for: Date())
                
                
                let components = calendar.dateComponents([.day], from: date1, to: date2)
                
                
                let dayDiff = components.day as! Int
                print(dayDiff)
                
                if dayDiff >= 0 {
                    sectionTitle = "Today"
                }
                else if dayDiff >= 7 {
                    sectionTitle = "Last Week"
                }
                else if dayDiff >= 30 {
                    sectionTitle = "Last Month"
                }
                else if dayDiff >= 120 {
                    sectionTitle = "Last 3 Month"
                }
                else if dayDiff >= 180 {
                    sectionTitle = "Last 6 Month"
                }
                else if dayDiff >= 365 {
                    sectionTitle = "1 Year"
                }
                
            }
        }
        if let val = json["createdByUser"] as? [String:Any]{
            self.createdByUser = User(json: val)
        }
        
    }
}


