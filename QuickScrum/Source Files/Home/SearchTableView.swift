//
//  SearchTableView.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 11/02/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit

class SearchTableView: UIView , UITableViewDelegate , UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet weak var  searchBar : UISearchBar!
    @IBOutlet weak var  tblProject : UITableView!
    @IBOutlet weak var searchFooter: SearchFooter!
    var projectArr : [Projects]!
    var filteredProjects = [Projects]()
    var parentVC : UIViewController!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        if tblProject != nil {
            tblProject.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
            tblProject.dataSource = self
            tblProject.delegate = self
        }
        if searchBar != nil {
            //searchResultsUpdater = self
           // searchController.obscuresBackgroundDuringPresentation = false
            searchBar.placeholder = "Search Project"
        }
       //  definesPresentationContext = true
        searchBar.scopeButtonTitles = ["All"]
        searchBar.delegate = self
        // Setup the search footer
        if searchFooter != nil , tblProject != nil {
             tblProject.tableFooterView = searchFooter
        }
        // Drawing code
    }
    
    //MARK:
    //MARK: UITableView Method
    //MARK:
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            searchFooter.setIsFilteringToShow(filteredItemCount: filteredProjects.count, of: projectArr.count)
            return filteredProjects.count
        }
        searchFooter.setNotFiltering()
        if self.projectArr != nil {
            return projectArr.count
        }
        return 0
        
       }
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50//UITableViewAutomaticDimension
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if projectArr != nil  {
                let  cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
                var project: Projects
                if isFiltering() {
                    project = filteredProjects[indexPath.row]
                } else {
                    project = projectArr[indexPath.row]
                }
                cell.textLabel?.text = project.name
                return cell
            }
            return UITableViewCell()
        }    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.frame = CGRect(x: 0, y: height, width: UIScreen.main.bounds.width, height: 0)
            if parentVC != nil {
                if let objVC = parentVC.storyboard?.instantiateViewController(withIdentifier: "Dashboard") {
                    parentVC.navigationController?.pushViewController(objVC, animated: true)
                }
            }
        }
    // MARK: - Private instance methods
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        if projectArr != nil {
            filteredProjects = projectArr.filter({( project : Projects) -> Bool in
                let doesCategoryMatch = (scope == "All")
                if searchBarIsEmpty() {
                    return doesCategoryMatch
                } else {
                    return doesCategoryMatch && project.name.lowercased().contains(searchText.lowercased())
                }
            })
            relaodTable()
        }
    }
    func searchBarIsEmpty() -> Bool {
        
        if (searchBar.text?.isEmpty)! {
            return true
        }
        else {
            return false
        }
        /*print(searchBar.text?.count)
        if Int(searchBar.text?.count) > 0 {
            return true
        }
        return false
        */
        //let s = String (format:"%@",searchBar.text!)
        //return s.count > 0 ?? false:true
        
       // return searchBar.text?.isEmpty ?? true; : false
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        filterContentForSearchText(self.searchBar.text!, scope: "All")
        return true
    }
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchBar.selectedScopeButtonIndex != 0
        // return true
        return (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    
    func relaodTable(){
        if tblProject != nil {
            tblProject.reloadData()
        }
    }
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print(self.searchBar.text!)                                                                                                                           
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(text, scope: scope)//self.searchBar.text!
        return true
    }
    
    
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(self.searchBar.text!, scope: scope)
    }
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }

}
