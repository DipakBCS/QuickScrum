//
//  CommonView.swift
//  QuickScrum
//
//  Created by Alite on 2/11/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit

class CommonView: UIView, UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var menuTable : CustomTableView!
    var fieldArray = [[String:Any]]()
    var user : User!
    let gradientLayer = CAGradientLayer()
    
    
    // Only override draw() if you perform custom drawing.
    
    override func draw(_ rect: CGRect) {
        if menuTable != nil {
            menuTable.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
            menuTable.register(UINib(nibName: "ProfileView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ProfileView")
            menuTable.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
            menuTable.dataSource = self
            menuTable.delegate = self
            menuTable.backgroundColor = WC
        }
        self.tag = VIEW_REMOVE_TAG
        addValuesInDictionary(val: "Notification", img: #imageLiteral(resourceName: "ic_notification"))
        addValuesInDictionary(val: "Dashboard", img: #imageLiteral(resourceName: "ic_dashboard"))
        addValuesInDictionary(val: "Projects", img: #imageLiteral(resourceName: "ic_planning"))
        addValuesInDictionary(val: "Planning", img: #imageLiteral(resourceName: "ic_planning"))
        addValuesInDictionary(val: "Profile", img: #imageLiteral(resourceName: "ic_profile"))
        addValuesInDictionary(val: "Log out", img: #imageLiteral(resourceName: "ic_logoff"))
        
        let swiftLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiftLeftClicked(gesture:)))
        swiftLeft.direction = .left
        self.addGestureRecognizer(swiftLeft)
        // Drawing code
    }
    
    func swiftLeftClicked(gesture:UISwipeGestureRecognizer){
        if gesture.direction == .left {
            print("gesture.direction == .left")
            self.fadeOut()
        }
    }
    func addValuesInDictionary(val:String,img:UIImage){
        let dic : [String:Any] = ["Name":val,
                                  "Image":img]
        fieldArray.append(dic)
    }
    
    
    //MARK:
    //MARK: UITableView METHOD
    //MARK:
    
    func reloadTable(){
        if menuTable != nil  {
            DispatchQueue.main.async(execute: {
                self.menuTable.reloadData()
                self.updateConstraints()
                self.layoutIfNeeded()
            })
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 200 
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50//UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 200//UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ProfileView") as? ProfileView{
            guard let usrObj = self.user else {
                print("UserObject is nil")
                return UIView()
            }
            /*headerView.backView?.backgroundColor = BC
            let colorTop = THEME_COLOR//UIColor(red: 192.0 / 255.0, green: 38.0 / 255.0, blue: 42.0 / 255.0, alpha: 1.0).cgColor
            let colorBottom = BC//UIColor(red: 35.0 / 255.0, green: 2.0 / 255.0, blue: 2.0 / 255.0, alpha: 1.0).cgColor
            gradientLayer.frame =  headerView.backView.bounds//CGRect(x: 0, y: 0, width: self.bounds.width, height: 200)
            gradientLayer.colors = [colorTop, colorBottom]
            gradientLayer.locations = [0.0, 0.15]
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
          //  headerView.backView?.layer.insertSublayer(gradientLayer, at: 0)
            //headerView.layer.insertSublayer(gradientLayer, at: 1)
            headerView.backView?.layer.addSublayer(gradientLayer)*/
           // headerView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "LaunchScreen"))
            headerView.profileImage.image = #imageLiteral(resourceName: "profileimg_men")
            headerView.lblName.text = usrObj.fullName
            headerView.lblEmail.text = usrObj.loginEmailId
            return headerView
        }
        return UIView()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let  cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as? HomeCell {
            let dic = fieldArray[indexPath.row]
            cell.lblTitle.text = (dic["Name"] as! String)
            cell.imgView.image = dic["Image"] as? UIImage
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    

}
