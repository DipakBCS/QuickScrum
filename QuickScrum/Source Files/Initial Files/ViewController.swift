//
//  ViewController.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 31/01/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController , UITextFieldDelegate {
    
    @IBOutlet weak var txtUsername : UITextField!
    @IBOutlet weak var txtPassword : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.layer.insertSublayer(Colors().gl, at: 0)
        self.txtUsername.text = "adi.manohar@gmail.com"
        self.txtPassword.text = "123"
        
    }
    
    @IBAction func btnLoginClicked(){
        print(self.isValidEmail(txtUsername.text!))
        if self.txtUsername == nil || (self.txtUsername.text?.isEmpty)! {
            self.showAlert(message: "Please enter your Username.")
        }
        else if self.txtPassword == nil || (self.txtPassword.text?.isEmpty)! {
            self.showAlert(message: "Please enter your Password.")
        }
        else if self.txtUsername == nil || !(self.isValidEmail(txtUsername.text!)) {
            self.showAlert(message: "Please enter valid email.")
        }
        else {
          //   navigateScreen()
            logInAPICall()
        }
    }
    
    func navigateScreen(){
        
     //   self.navigationController?.navigationItem.hidesBackButton = true
     //   self.navigationController?.navigationBar.isHidden = true
       /* if let objVC = self.storyboard?.instantiateViewController(withIdentifier: "ProjectListVC") {
            self.navigationController?.pushViewController(objVC, animated: true)
        }*/
        self.performSegue(withIdentifier: "HomeVC", sender: nil)
    }
    
    
    //MARK:
    //MARK: UITextField  Method
    //MARK:
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField.returnKeyType == .next){
            textField.resignFirstResponder()
            self.txtPassword.becomeFirstResponder()
        }else if(textField.returnKeyType == .done){
            btnLoginClicked()
            
        }
        return true
    }
    
    //MARK:
    //MARK: Server Communication Method
    //MARK:
    
    
    
    
    func logInAPICall(){
        if GlobalClass.sharedInstance.checkInternet().boolValue{
                let parameters  = [
                    "email": String(format:"%@",self.txtUsername.text!),
                    "password": String(format:"%@",self.txtPassword.text!)
                ] as [String : Any]
            //print(parameters)
                self.startLoading()
                let str = "\(BASE_URL)/api/v1/Authentication"
                Alamofire.request(str, method: .post, parameters: parameters, encoding: JSONEncoding.init(options: .init(rawValue: 0))).responseJSON { (responseData) -> Void in                    
                //    print(responseData.result.value)
                    switch responseData.result {
                    case .success:
                        dLog("Validation Successful")
                       // print(responseData.result.value)
                        if let responceDic = responseData.result.value as? [String: Any] {                           
                            if let tok = responceDic["resultData"] as? String {
                               // let tok =  "Basic c09pd0FiNGhnZGw4bTZuWHdoSFA0Wnk3RDZ5VHJpdS9WVFZwVlNxRGZGND06YWRpLm1hbm9oYXJAZ21haWwuY29tOjE6MTo4OjE6NjM2NTQwMjE3MzA4NjE0Nzcx"
                                UserDefaults.standard.set(tok, forKey: TOKEN)
                                UserDefaults.standard.set(true, forKey: IS_LOGIN)
                                UserDefaults.standard.synchronize()                               
                            }
                            DispatchQueue.main.async(execute: {
                                self.stopLoading()
                                self.performSegue(withIdentifier: "HomeVC", sender: nil)
                            })
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
        }
        else {
            self.showAlert(message: CHECK_INTERNET)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

