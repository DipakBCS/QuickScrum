//
//  AppDelegate.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 31/01/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit
import Firebase

import UserNotifications

var height : CGFloat!
enum ThemeType: Int {
    case defaultTheme = 0
    case pinkTheme = 1
}
var kTheme = Theme()
let THEME_TYPE = "THEME_TYPE"
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,UNUserNotificationCenterDelegate,MessagingDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {        
        print("didFinishLaunchingWithOptions called")
        /*FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")*/
        // Override point for customization after application launch.
        
        setTheme()
        
        guard let isLogin = UserDefaults.standard.value(forKey: IS_LOGIN) as? Bool else {return false}
        if isLogin {
            let viewController:UINavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! UINavigationController
            window!.rootViewController = viewController
        }
        else {
          
        }
        return true
    }
    
    func configureNotification(){
       
    }
    func setTheme() {
        if let themeID = UserDefaults.standard.value(forKey: THEME_TYPE) as? Int {
            //  0 for default theme , 1 for pink theme
            kTheme = Theme().setThemeWith(themeTyp: themeID)
        } else {
            //  0 for default theme , 1 for pink theme
            kTheme = Theme().setThemeWith(themeTyp: 0)
            UserDefaults.standard.setValue(0, forKey: THEME_TYPE)
            UserDefaults.standard.synchronize()
        }
        self.window?.tintColor = THEME_COLOR//kTheme.themeColor
        
        UIApplication.shared.isStatusBarHidden = kTheme.statusBarHidden
        UIApplication.shared.statusBarStyle = kTheme.statusBarStyle
       // UINavigationBar.appearance().barTintColor = kTheme.navbarTintColor
       // UINavigationBar.appearance().isTranslucent = kTheme.translucent
      //  UINavigationBar.appearance().tintColor = kTheme.navTintColor
        
      //  UIToolbar.appearance().tintColor = kTheme.themeColor
      //  UITabBar.appearance().tintColor = kTheme.themeColor
     //   UITextField.appearance().tintColor = kTheme.searchBarTintColor
       
        
       // UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: kTheme.navTitleColor]
      //  UISwitch.appearance().onTintColor = kTheme.themeColor
        
       // UISearchBar.appearance().tintColor = kTheme.searchBarTintColor
      //  UISwitch.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).onTintColor = kTheme.switchInNavTintColor
      //  UISegmentedControl.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).tintColor = kTheme.segmentTintColor // On/off in Workout
      //  UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).setTitleTextAttributes([NSForegroundColorAttributeName: kTheme.barButtonDisable], for: .disabled)
      //  UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).setTitleTextAttributes([NSForegroundColorAttributeName: kTheme.barButtonEnable], for: .normal)
       // UILabel.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).textColor = kTheme.navTitleColor
        if #available(iOS 11.0, *) {
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSForegroundColorAttributeName: kTheme.searchBarTintColor]
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: "", attributes: [NSForegroundColorAttributeName: kTheme.searchBarTintColor])
        }
        
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    private func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
class Theme {
    var themeColor = BLACK_COLOR
    var themeType: ThemeType?
    var translucent = true
    var statusBarHidden = false
    var statusBarStyle = UIStatusBarStyle.default
    var segmentTintColor = BLACK_COLOR
    var searchBarTintColor = BLACK_COLOR
    var navTitleColor = BLACK_COLOR
    var navTintColor = BLACK_COLOR
    var switchInNavTintColor = BLACK_COLOR
    var labelTitleColor = BLACK_COLOR
    var navbarTintColor = WC
    var barButtonDisable = LGC
    var barButtonEnable = BLACK_COLOR
    var userChatBlockCol = LGC
    
    func setThemeWith(themeTyp: Int) -> Theme {
        self.themeType = ThemeType(rawValue: themeTyp)
        switch self.themeType {
        case .defaultTheme?:
            themeColor = BLACK_COLOR
            translucent = true
            statusBarHidden = false
            statusBarStyle = UIStatusBarStyle.default
            segmentTintColor = BLACK_COLOR
            searchBarTintColor = BLACK_COLOR
            navTitleColor = BLACK_COLOR
            navTintColor = BLACK_COLOR
            switchInNavTintColor = BLACK_COLOR
            labelTitleColor = BLACK_COLOR
            navbarTintColor = WC
            barButtonDisable = LGC
            barButtonEnable = BLACK_COLOR
            print("Default theme")
            
        case .pinkTheme?:
            translucent = false
            themeColor = PINK_COLOR
            statusBarStyle = .lightContent
            themeColor = PINK_COLOR
            segmentTintColor = WC
            searchBarTintColor = WC
            navTitleColor = WC
            switchInNavTintColor = .green
            labelTitleColor = WC
            navbarTintColor = PINK_COLOR
            navTintColor = WC
            barButtonEnable = WC
        default:
            break
        }
        return self
    }
}
