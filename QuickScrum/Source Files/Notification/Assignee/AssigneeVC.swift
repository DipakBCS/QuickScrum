//
//  AssigneeVC.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 05/04/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit

class AssigneeVC: UIViewController , UITableViewDataSource , UITableViewDelegate{
    
    @IBOutlet weak var tblPriority : UITableView!
    let priorityArr = ["John Smith","Rohit Verma","John","David ","Johnshon","Donald"]
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        if tblPriority != nil {
            tblPriority.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        }
    }
    
    @IBAction func btnCancelClicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return priorityArr.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = priorityArr[indexPath.row]
        cell.tintColor = THEME_COLOR
        cell.imageView?.image = #imageLiteral(resourceName: "ic_notification")
        if indexPath.row == 0 {
            cell.accessoryType = .checkmark
        }
        else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellArr = tableView.visibleCells
        for cell in cellArr {
            cell.accessoryType = .none
        }
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
    }
}

