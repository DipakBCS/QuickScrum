//
//  AttachmentCell.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 07/04/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit

class AttachmentCell: UITableViewCell , UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var pageControl : UIPageControl!
    var imgArr : [UIImage]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func reloadCollectionView(){
        if collectionView != nil {
            collectionView.register(UINib(nibName: "ImageCell0", bundle: nil), forCellWithReuseIdentifier:"ImageCell0")
            collectionView.dataSource = self
            let layout = UICollectionViewFlowLayout()
            layout.itemSize = CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
            layout.minimumLineSpacing = 0
            layout.scrollDirection = .horizontal
            collectionView.collectionViewLayout = layout
            collectionView.reloadData()
        }
        if pageControl != nil && imgArr != nil  {
            pageControl.numberOfPages = imgArr.count
        }
    }
    
    func numberOfSections(in _: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if imgArr != nil {
            return imgArr.count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout _: UICollectionViewLayout, sizeForItemAt _: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, minimumInteritemSpacingForSectionAt _: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell0", for: indexPath) as? ImageCell0 {
            if imgArr.count > indexPath.row {
                cell.imageFull.image = imgArr[indexPath.row]
            }
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_: UICollectionView, willDisplay _: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
