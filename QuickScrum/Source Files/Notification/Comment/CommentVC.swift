//
//  CommentVC.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 20/03/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire

class CommentVC: UIViewController , UITableViewDataSource , UITableViewDelegate , DropDownProtocol,ImagePickerDelegate {
    
    let Assignee = "Assignee"
    let Priority = "Priority"
    let Attachment = "Attachment"

    @IBOutlet weak var tblComment : UITableView!
    @IBOutlet weak var txtComment : UITextView!
    @IBOutlet weak var lblPlaceHolder : UILabel!
    @IBOutlet var  footerView                : UIVisualEffectView!
    @IBOutlet weak var heightCon : NSLayoutConstraint!
    let sectionArr = ["Detail","Attachment","Activity","Time Sheet","Comments"]
    var imgArr = [UIImage]()
    var rowArr = [[String:String]]()
    
    var notification : Notification!
    override func viewDidLoad() {
        super.viewDidLoad()
         rowArr = [
            ["Header" : "Description" , "Value":"This is sample Task"],
            ["Header" : "Issue Type", "Value":"This is sample Task"],
            ["Header" : Priority, "Value":"Medium"],
            ["Header" : Assignee, "Value":"John Smith"],
            ["Header" : "Created On", "Value":"3 April 2018 6:30 pm"],
            ["Header" : "Updated By","Value":"4 April 2018 5:50 AM"]
        ]
        
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShow(notification:)), name:.UIKeyboardWillShow, object:nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillHide(notification:)), name:.UIKeyboardWillHide, object:nil)
        if tblComment != nil {
            tblComment.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
            tblComment.register(UINib(nibName: "DetailCell1", bundle: nil), forCellReuseIdentifier: "DetailCell1")
            tblComment.register(UINib(nibName: "DropDownCell", bundle: nil), forCellReuseIdentifier: "DropDownCell")
            tblComment.register(UINib(nibName: "DetailCell0", bundle: nil), forCellReuseIdentifier: "DetailCell0")
            tblComment.register(UINib(nibName: "AttachmentCell", bundle: nil), forCellReuseIdentifier: "AttachmentCell")
            tblComment.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
            tblComment.register(UINib(nibName: "LabelHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "LabelHeaderView")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if tblComment != nil {
            let indexPath = tblComment.indexPathForSelectedRow
            if indexPath != nil {
                self.tblComment.deselectRow(at: indexPath!, animated: true)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK:
    // MARK: - Server Communication Methods
    // MARK:
    func getTaskComments(){
        if GlobalClass.sharedInstance.checkInternet().boolValue{
            let token = UserDefaults.standard.value(forKey: TOKEN) as? String
            if token != nil,!(token?.isEmpty)!, notification != nil , notification.itemId != nil  {
                // self.startLoading() http://qsrapi.azurewebsites.net/api/v1/Epic/382/comment
                let url = String(format: "%@/api/v1/Epic/%d/comment", BASE_URL,notification.itemId)
                Alamofire.request(url, method: .get, parameters:nil,encoding: JSONEncoding.default, headers: GlobalClass.sharedInstance.authorizationHeader).responseJSON {
                    response in
                    //    self.hideLoading()
                   // print(response.result.value)
                    switch response.result {
                    case .success:
                        guard let dic =  response.result.value as? [String:Any]else {
                            print("responce is not dictionary")
                            return
                        }
                        if let _ = dic["user"] as? [String:Any]{
                        }
                        break
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }
        else {
            self.showAlert(message: CHECK_INTERNET)
        }
    }
    
    func sendComments(comment:String){
        if GlobalClass.sharedInstance.checkInternet().boolValue{
            let token = UserDefaults.standard.value(forKey: TOKEN) as? String
            if token != nil , !(token?.isEmpty)! {
                self.startLoading()
                let param  = [
                    "name": comment,
                    ] as [String : Any]
               // http://qsrapi.azurewebsites.net/api/v1/Epic/382/comment
                let url = String(format: "%@/api/v1/Epic/%d/comment", BASE_URL,notification.itemId)
                Alamofire.request(url, method: .post, parameters:param,encoding: JSONEncoding.default, headers: GlobalClass.sharedInstance.authorizationHeader).responseJSON {
                    response in
                    self.hideLoading()
                    print("sendComments completed")
                   // print(response.result.value)
                    switch response.result {
                    case .success:
                        if let arr1 = response.result.value as? [String:Any]  {
                            if let arr = arr1["resultData"] as? [[String:Any]], arr.count > 0 {
                            }
                        }
                        break
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }
        else {
            self.showAlert(message: CHECK_INTERNET)
        }
    }
    
    // MARK:
    // MARK: - Keyboard Methods
    // MARK:
    
    func keyboardWillShow(notification:NSNotification){
        if let userInfo = notification.userInfo{
            if let keyboardSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size{
                if heightCon != nil {
                    heightCon.constant = keyboardSize.height
                    if #available(iOS 11.0, *) {
                        heightCon.constant = keyboardSize.height - view.safeAreaInsets.bottom
                    } else {
                        heightCon.constant = keyboardSize.height
                        // Fallback on earlier versions
                    }
                }
                if self.tblComment != nil {
                    self.tblComment.contentOffset.y = self.tblComment.contentSize.height
                }
            }
        }
    }
    func keyboardWillHide(notification: NSNotification) {
        if heightCon != nil {
            heightCon.constant = 0
        }
       // isKeyboardOpen = false
    }
    
    @IBAction func  btnCancelClicked(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSendClicked(){
        if txtComment != nil && !txtComment.text.isEmpty {
            let comment = self.txtComment.text.trimmingCharacters(in: .whitespacesAndNewlines)
            if comment != "" {
                self.sendComments(comment: comment)
            }
            else {
                self.showAlert(message: "Please enter a comment")
            }
        }
    }
    // MARK:
    // MARK: - UITableView  Methods
    // MARK:
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArr.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return rowArr.count
        }
        else if section == 1 {
            if imgArr.count > 0 {
                return 1
            }
        }
        else if section == 4 {
            return 3
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 22
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0  || indexPath.section == 4 {
            return UITableViewAutomaticDimension
        }
        if indexPath.section == 1 {
            return 200
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LabelHeaderView") as? LabelHeaderView{
            headerView.lblHeader.text = sectionArr[section]
            if sectionArr[section] == Attachment {
                if headerView.btnImage != nil {
                     headerView.btnImage.setImage(UIImage(named: "ic_attachment"), for: .normal)
                    headerView.btnImage.addTarget(self, action: #selector(btnAttachmentClicked(button:)), for: .touchUpInside)
                }
            }
            else {
                headerView.btnImage.setImage(UIImage(), for: .normal)
                //headerView.btnImage.setImage(UIImage(named: "ic_rightArrow"), for: .normal)
            }
            return headerView
        }
        return UIView()
    }
    
    func btnAttachmentClicked(button:UIButton){
        let imagePicker = CustomImagePicker()
        imagePicker.imagePickerDelegate = self
        imagePicker.vc = self
        imagePicker.initPicker()
    }
    func returnPickerImage(_ img:UIImage){
        self.imgArr.append(img)
        self.reloadMainTable()
    }
    func reloadMainTable() {
        if self.tblComment != nil {
             self.tblComment.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell0", for: indexPath) as? DetailCell0 {
                    cell.selectionStyle = .none
                    return cell
                }
            }
            else if indexPath.row == 1 {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownCell", for: indexPath) as? DropDownCell {
                    cell.selectionStyle = .none
                    cell.dropDownDelegate = self                    
                    return cell
                }
            }else if indexPath.row == 2 {
                 let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
                    cell.textLabel?.text = "Priority Medium"
                    cell.accessoryType = .disclosureIndicator
                    let dic = rowArr[indexPath.row]
                    if dic["Header"] == Priority {
                    }
                
                    return cell
            }
            else if let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell1", for: indexPath) as? DetailCell1 {
                if rowArr.count > indexPath.row {
                    let dic = rowArr[indexPath.row]
                    cell.lblName.text = dic["Header"]
                    cell.lblDesc.text = dic["Value"]
                    cell.imgView.image = UIImage(named: "ic_setting")
                    if dic["Header"] == Assignee {
                        cell.accessoryType = .disclosureIndicator
                    }
                    return cell
                }
                return UITableViewCell()
            }
        }
        else if indexPath.section == 4 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as? CommentCell {
                if notification != nil {
                    cell.lblName.text = notification.createdByUser.fullName
                    cell.lblDate.text = notification.createdDate
                    cell.lblComment.text = "This is test comment.This is test comment. This is test comment.This is test comment. This is test comment."//notification.comment
                }
                return cell
            }
        }
        else if indexPath.section == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "AttachmentCell", for: indexPath) as? AttachmentCell {
                cell.imgArr = imgArr
                cell.reloadCollectionView()
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
                let dic = rowArr[indexPath.row]
                if dic["Header"] == Priority {
                    if let objVC = self.storyboard?.instantiateViewController(withIdentifier: "PriorityVC") as? PriorityVC {                    self.navigationController?.pushViewController(objVC, animated: true)
                    }
                }
                else if dic["Header"] == Assignee {
                    if let objVC = self.storyboard?.instantiateViewController(withIdentifier: "AssigneeVC") as? AssigneeVC {                    self.navigationController?.pushViewController(objVC, animated: true)
                    }
                }
            }
   }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if self.lblPlaceHolder != nil {
            self.lblPlaceHolder.isHidden = textView.text.count <= 0 ? false : true
        }
        if text == "\n"{
        }
        return true
    }
    
}
