//
//  CommentView.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 24/03/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit

class CommentView: UIView {
    
    @IBOutlet weak var txtComment : UITextView!
    @IBOutlet weak var lblPlaceHolder : UILabel!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
