//
//  DropDownCell.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 29/03/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit
protocol DropDownProtocol {
    func reloadMainTable()
}


class DropDownCell: UITableViewCell,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var btnDropDown : UIButton!
    @IBOutlet weak var tblDropDown : UITableView!
    @IBOutlet weak var tblHeight : NSLayoutConstraint!
    var dataSourceArr = ["To Do","In Progress","Internal Testing","Completed"]
    var originalHeight : CGFloat = 0
    
    var dropDownDelegate : DropDownProtocol!
    override func awakeFromNib() {
        super.awakeFromNib()
        if btnDropDown != nil {
            btnDropDown.layer.cornerRadius = 05
            self.btnDropDown.setTitle(String(format:"  %@  ",dataSourceArr[0]) , for:.normal)
            self.btnDropDown.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
        }
        if tblHeight != nil {
            originalHeight = tblHeight.constant
        }
        tblHeight.constant = 0
        if tblDropDown != nil {
            tblDropDown.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
            tblDropDown.dataSource =  self
            tblDropDown.delegate = self
        }
    }
    func reloadTable(){
        DispatchQueue.main.async(execute: {
            if self.tblHeight != nil {
                if self.tblHeight.constant == 0 {
                    self.tblHeight.constant = self.originalHeight
                }
                else {
                    self.tblHeight.constant = 0
                }
            }
            if self.tblDropDown != nil {             
                self.tblDropDown.reloadData()
                self.tblDropDown.updateConstraints()
                self.updateConstraints()
            }
            if self.dropDownDelegate != nil {
                self.dropDownDelegate.reloadMainTable()
            }
        })
    }
    func btnDropDownClicked(sender:UIButton){
        self.reloadTable()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArr.count
    }
   /* func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }*/
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = dataSourceArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.btnDropDown.setTitle(String(format:"  %@  ",dataSourceArr[indexPath.row]) , for:.normal)
        self.reloadTable()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
