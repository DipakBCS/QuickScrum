//
//  NotificationCell.swift
//  
//
//  Created by Bharti Soft Tech Pvt Ltd on 27/02/18.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblAssignee : UILabel!
    @IBOutlet weak var lblTaskId : UILabel!
    @IBOutlet weak var lblCreatedDate : UILabel!
    @IBOutlet weak var imgProfile : UIImageView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        if self.imgProfile != nil {            
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
