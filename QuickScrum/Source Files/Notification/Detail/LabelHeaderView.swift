//
//  LabelHeaderView.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 02/04/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit

class LabelHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var lblHeader : UILabel!
    @IBOutlet weak var btnImage : UIButton!
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
