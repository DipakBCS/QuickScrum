//
//  ExtensionClasses.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 05/02/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func showAlert(message: String) {
        let alert = UIAlertController(title: Alert, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true, completion: nil)
        })
    }
    
   
    func setLoginFlag(){
        UserDefaults.standard.set(true, forKey: IS_LOGIN)
        UserDefaults.standard.synchronize()
    }
     /*Email Validation*/
    func isValidEmail(_ testStr:String) -> Bool {
        dLog("validate emilId: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    func startIndicator(){
        DispatchQueue.main.async(execute: {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
        })
    }
    func stopIndicator(){
        DispatchQueue.main.async(execute: {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        })
    }
    
    func startLoading(){
        DispatchQueue.main.async(execute: {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
             GlobalClass.sharedInstance.initiateLoaderWithMessage(inView: self.view)
        })
    }
    func stopLoading(){
        DispatchQueue.main.async(execute: {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            GlobalClass.sharedInstance.HideLoader()
        })
    }

}
