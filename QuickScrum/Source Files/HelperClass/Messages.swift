//
//  Messages.swift
//  TangerinePatientLive
//
//  Created by SYSMeta IT on 11/2/16.
//  Copyright © 2016 SYSMeta IT. All rights reserved.
//

import Foundation


let Alert = NSLocalizedString("Alert", comment: "")
let INVALID_UN = NSLocalizedString("Please enter a username", comment: "Please enter a username")
let LOGOUT_MSG = NSLocalizedString("Do you want to logout?", comment: "Do you want to logout?")

let OK  = NSLocalizedString("OK", comment: "")



let Success 		 = NSLocalizedString("Success", comment: "")

let Oops = NSLocalizedString("Oops", comment: "")
let SOME_THING_WRONG = NSLocalizedString("Some thing went wrong!!!", comment: "")
let Turn_On_Notifications = NSLocalizedString("Turn on notifications", comment: "")
let Turn_On_Notifications_Message = NSLocalizedString("This app needs notifications turned on for the best user experience.", comment: "")

let months = [
                NSLocalizedString("Jan", comment: "Jan"),
                NSLocalizedString("Feb", comment: "Feb"),
                NSLocalizedString("Mar", comment: "Mar"),
                NSLocalizedString("Apr", comment: "Apr"),
                NSLocalizedString("May", comment: "May"),
                NSLocalizedString("Jun", comment: "Jun"),
                NSLocalizedString("Jul", comment: "Jul"),
                NSLocalizedString("Aug", comment: "Aug"),
                NSLocalizedString("Sep", comment: "Sep"),
                NSLocalizedString("Oct", comment: "Oct"),
                NSLocalizedString("Nov", comment: "Nov"),
                NSLocalizedString("Dec", comment: "Dec")
             ]
let EMPTY_USERNAME   = NSLocalizedString("empty username",   comment: "empty username")
let EMPTY_PASSWORD   = NSLocalizedString("empty password",   comment: "empty password")
let INVALID_USERNAME = NSLocalizedString("invalid userName", comment: "invalid userName")
let INVALID_PASSWORD = NSLocalizedString("invalid password", comment: "invalid password")
let INVALID_USERNAME_PASSWORD = NSLocalizedString("invalid username password", comment: "invalid username password")

let Calories         = NSLocalizedString("calories", comment: "calories")
let Carbohydrates    = NSLocalizedString("Carbohydrates", comment: "Carbohydrates")
let Energy           = NSLocalizedString("Energy", comment: "Energy")
let Lipid            = NSLocalizedString("Lipid", comment: "Lipid")
let Protine          = NSLocalizedString("Protine", comment: "Protine")

let INTERNET_NOT_AVAILABLE = NSLocalizedString("internet Notavailable", comment: "internet Notavailable")

let AGE_VALIDATION = NSLocalizedString("You need to be atleast 13 year old to use TangerineLive Patient.",   comment: "")

let COATCH_SELECTION = NSLocalizedString("Do you wants to send a request to this user?",   comment: "")

let HEIGHT_VALIDATION			=  NSLocalizedString("Please enter your height.", comment: "")

let WEIGHT_VALIDATION			=  NSLocalizedString("Please enter your weight.", comment: "")

let PASSWORD_VALIDATION    		=  NSLocalizedString("Please enter password.", comment: "")

let PASSWORD_AGAIN_VALIDATION	=  NSLocalizedString("Please enter password again.", comment: "")

let PASSWORD_MATCH_VALIDATION	=  NSLocalizedString("Password does not match", comment: "")

let ENTER_MEAL_NAME         	=  NSLocalizedString("Enter meal Name", comment: "")

let ENTER_MEAL_NAME2         	=  NSLocalizedString("Please enter meal name", comment: "")

let SPECIAL_CHARACTER_NOT_ALLOWED = NSLocalizedString("Special characters not allowed in meal name.", comment: "")

let CHART_DATA_VALIDATION		= NSLocalizedString("No chart data available.", comment: "")

let PLACE_HOLDER_TEXT 			= NSLocalizedString("New Message", comment: "") // Nouveau message

let Cardio           			= NSLocalizedString("Cardio", comment: "Cardio")

let Strength         			= NSLocalizedString("Strength", comment: "Strength")

let Weight_str       			= NSLocalizedString("Weight", comment: "")

let Body_Fat_Percentage 		= NSLocalizedString("Body Fat Percentage", comment: "")

let Visceral_Fat_Level  		= NSLocalizedString("Visceral Fat Level", comment: "")

let Abdominal_Circumference 	= NSLocalizedString("Abdominal Circumference", comment: "")

let Calf_Circumference 			= NSLocalizedString("Calf Circumference", comment: "")

let Chest_Circumference 		= NSLocalizedString("Chest Circumference", comment: "")

let Hip_Circumference 			= NSLocalizedString("Hip Circumference", comment: "")

let Neck_circumference          = NSLocalizedString("Neck circumference", comment: "")

let Upper_Arm_Circumference 	= NSLocalizedString("Upper Arm Circumference", comment: "")

let Height = NSLocalizedString("Height", comment: "")

//Localise add remain
let menstrualDate = NSLocalizedString("menstrualDate", comment: "")

let leftArmCircumference = NSLocalizedString("leftArmCircumference", comment: "")

let leftThighCircumference = NSLocalizedString("leftThighCircumference", comment: "")

let leftUpperArmCircumference = NSLocalizedString("leftUpperArmCircumference", comment: "")

let product = NSLocalizedString("product", comment: "")

let rightArmCircumference = NSLocalizedString("rightArmCircumference", comment: "")

let rightThighCircumference = NSLocalizedString("rightThighCircumference", comment: "")

let rightUpperArmCircumference = NSLocalizedString("rightUpperArmCircumference", comment: "")

let visceralFatlevel = NSLocalizedString("visceralFatlevel", comment: "")

let Survey_Saved = NSLocalizedString("Survey is saved successfully.", comment: "")

let Image_Uploaded = NSLocalizedString("Images are uploaded successfully.", comment: "")
let Image_Uploaded_Failed = NSLocalizedString("Failed", comment: "")

let beforeTwohourMsg = NSLocalizedString("IF: 2 hours remaining", comment: "")
let timerOverAlert = NSLocalizedString("IF: duration is over!", comment: "")


