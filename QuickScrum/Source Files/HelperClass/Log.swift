//
//  Log.swift
//  TangerinePatientLive
//
//  Created by user on 17/11/16.
//  Copyright © 2016 SYSMeta IT. All rights reserved.
//

import Foundation

#if DEBUG
    func dLog(_ message: Any?, filename: String = #file, function: String = #function, line: Int = #line) {
        NSLog("\(filename) [Line \(line)] \(function) - \(String(describing: message))")
    }
#else
    func dLog(_ message: Any?, filename: String = #file, function: String = #function, line: Int = #line) { }
#endif
func aLog(message: Any?, filename: String = #file, function: String = #function, line: Int = #line) {
    NSLog("\(filename) [Line \(line)] \(function) - \(String(describing: message))")
}
