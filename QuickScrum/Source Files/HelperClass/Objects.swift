//
//  Objects.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 10/02/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import Foundation


class User {
    var fullName : String?
    var id : Int!
    var loginEmailId : String?
    var nameInitials : String?
    var typeId : Int?
    
    init(json:[String:Any]){
        if let val  = json["fullName"] as? String {
            self.fullName = val
        }
        if let val  = json["id"] as? Int {
            self.id = val
        }
        if let val  = json["loginEmailId"] as? String {
            self.loginEmailId = val
        }
        if let val  = json["nameInitials"] as? String {
            self.nameInitials = val
        }
        if let val  = json["typeId"] as? Int {
            self.typeId = val
        }
    }
}

class Projects {
    
    var id : Int!
    var companyId : Int!
    var createdDate : String?
    var name : String!
    var  projectTypeId : Int?
    var statusId : Int?
    var typeId : Int?
    
      init(json:[String:Any]){    
        if let val  = json["name"] as? String {
            self.name = val
        }
        if let val  = json["id"] as? Int {
            self.id = val
        }
        if let val  = json["companyId"] as? Int {
            self.companyId = val
        }
    }
    
    
    
    
}
