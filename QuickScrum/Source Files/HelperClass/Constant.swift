//
//  Constant.swift

//
//  Created by SYSMeta IT on 29/09/15.
//  Copyright © 2015 SYSMeta IT. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

let RC  = UIColor.red
let OC  = UIColor.orange
let WC  = UIColor.white
let BC  = UIColor.black
let BLC = UIColor.blue
let YC  = UIColor.yellow
let GC  = UIColor.green
let PC  = UIColor.purple
let CC  = UIColor.clear
let LGC = UIColor.lightGray
let DGC = UIColor.darkGray
let GRAY_C = UIColor(red: 234.0/255, green: 234.0/255, blue: 241.0/255, alpha: 1)
let DEFAULT_RED = UIColor(red: 223.0/255, green: 48.0/255, blue: 68.0/255, alpha: 1)
let THEME_COLOR = UIColor(red: 92.0/255, green: 192.0/255, blue: 165.0/255, alpha: 1)



let LOGO_IMG = "imgo"
let USER_IMG = "user"
let LOCK_IMG = "lock"
let ResultMedical_SearchResult = "ResultMedical_SearchPatientResult"

let Cancel = "Cancel"


let VIEW_REMOVE_TAG = 500
let BLACK_COLOR = UIColor.black




let BACK_COLOR1 	 = UIColor(red: 235.0/255.0, green: 138.0/255.0, blue: 214/255.0, alpha: 1.0)
let BACK_COLOR2 	 = UIColor(red: 171.0/255.0, green: 220.0/255.0, blue: 122/255.0, alpha: 1.0)
let BACK_COLOR3 	 = UIColor(red: 129.0/255.0, green: 204.0/255.0, blue: 216/255.0, alpha: 1.0)
let BACK_COLOR4 	 = UIColor(red: 255.0/255.0, green: 109.0/255.0, blue: 112/255.0, alpha: 1.0)
let LIGHT_GRAY_COLOR = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243/255.0, alpha: 1.0)
let LIGHT_GRAY_COLOR2 = UIColor(red: 241.0/255.0, green: 241.0/255.0, blue: 241/255.0, alpha: 1.0)
let NAV_BLUE_COLOR   = UIColor(red: 23.0/255.0,  green: 121.0/255.0, blue: 182/255.0, alpha: 1.0)
let DARK_GRAY_COLOR  = UIColor(red: 75.0/255.0,  green: 75.0/255.0,  blue: 75.0/255.0, alpha: 1.0)
let TABLE_BACK_COLOR = UIColor(red: 227.0/255.0, green: 227.0/255.0, blue: 227.0/255.0, alpha: 1.0)

let CHART_LINES = UIColor(red: 227.0/255.0, green: 227.0/255.0,  blue: 227.0/255.0, alpha: 1.0)
let CHART_COLOR1 = UIColor(red: 60.0/255.0,  green: 187.0/255.0, blue: 202/255.0, alpha: 1.0)//R: 60 G: 187 B: 202
let CHART_COLOR2 = UIColor(red: 78.0/255.0,  green: 196.0/255.0, blue: 165/255.0, alpha: 1.0)//R: 78 G: 196 B: 165
let CHART_COLOR3 = UIColor(red: 241.0/255.0, green: 201.0/255.0, blue: 116/255.0, alpha: 1.0)//R: 241 G: 201 B: 116
let CHART_COLOR4 = UIColor(red: 252.0/255.0, green: 144.0/255.0, blue: 98/255.0,  alpha: 1.0)//R: 252 G: 144 B: 98
let CHART_COLOR5 = UIColor(red: 255.0/255.0, green: 122.0/255.0, blue: 128/255.0, alpha: 1.0)//R: 	R: 255 G: 122 B: 128

let CHART_COLOR0 = UIColor(red: 239.0/255.0, green: 230.0/255.0, blue: 228/255.0, alpha: 1.0)//R: R: 239 G: 230 B: 228
let TAG_COLOR = UIColor(red: 52.0/255.0, green: 147.0/255.0, blue: 197.0/255.0, alpha: 1.0)
let PINK_COLOR = UIColor(red: 228.0/255.0, green: 62.0/255.0, blue: 86.0/255.0, alpha: 1.0)
let LIPID_COLOR = UIColor(red: 87.0/255.0, green: 87.0/255.0, blue: 87.0/255.0, alpha: 1.0)
let CARBO_COLOR = UIColor(red: 57.0/255.0, green: 57.0/255.0, blue: 57.0/255.0, alpha: 1.0)
let DARK_GREY_COLOR = UIColor(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0)


let APP_NAME = "Quick Scrum"

let  CHECK_INTERNET = "Please check your internet connection."
let TOKEN = "TOKEN"
let IS_LOGIN = "IS_LOGIN"

struct Errors {
    static let apiOperationFailed = NSError(domain: "Nutriligent", code: 42, userInfo:[NSLocalizedDescriptionKey: NSLocalizedString("api_operation_failed", comment: "")])
    static let unexpectedValue = NSError(domain: "Nutriligent", code: 43, userInfo:[NSLocalizedDescriptionKey: NSLocalizedString("api_operation_failed", comment: "")])
}

struct CameraErrors {
    static let cameraPermission_denied_inProfile = NSError(domain: "Nutriligent", code: 42, userInfo:[NSLocalizedDescriptionKey: NSLocalizedString("cameraPermission_denied_inProfile", comment: "")])
    static let cameraPermission_denied_inBarcodeScan = NSError(domain: "Nutriligent", code: 43, userInfo:[NSLocalizedDescriptionKey: NSLocalizedString("cameraPermission_denied_inBarcodeScan", comment: "")])
     static let cameraPermission_denied_inFoodFactScreen = NSError(domain: "Nutriligent", code: 44, userInfo:[NSLocalizedDescriptionKey: NSLocalizedString("cameraPermission_denied_inFoodFactScreen", comment: "")])
     static let cameraPermission_denied_inWorkout = NSError(domain: "Nutriligent", code: 45, userInfo:[NSLocalizedDescriptionKey: NSLocalizedString("cameraPermission_denied_inWorkout", comment: "")])
    
   
}

