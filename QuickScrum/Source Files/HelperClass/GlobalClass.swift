    //
//  GlobalClass.swift
//
//

import Foundation
import UIKit
import AVFoundation
import CoreLocation
import SDWebImage
import Alamofire
import Photos


protocol AsynchImage {
    func imageDidLoad(image: UIImage)
}
protocol CameraPermission
{
	func performActionAfterCameraPermission(_ permission:Bool)
}
  
class GlobalClass: NSObject, UIAlertViewDelegate, UITextFieldDelegate  {
    
    static let sharedInstance = GlobalClass()
    var viewController = UIViewController()
    
    var errorView:UIView!
    var isDefaultLatLongSet:Bool = false
    var imageCache = Dictionary<String, UIImage>()
    var cache:NSCache<AnyObject, AnyObject>!
    var session = URLSession.shared
    var alertMsg            = UIAlertController()
    var imgLoadingBG: UIImageView!
    var viewLoader: UIView?
    
    var authorizationHeader: HTTPHeaders {
        let str = String(format:"Basic %@",UserDefaults.standard.value(forKey: TOKEN) as! CVarArg)
        if !str.isEmpty { //  if let token = str.data(using: .utf8) {
            return ["Authorization": str]
        }
        return ["Authorization": ""]
    }
    
    override init() {
        super.init()
        self.cache = NSCache()
    }
    //For checking Internet
    func checkInternet() -> ObjCBool {
        
        if Reachability.isConnectedToNetwork() == true{
            return true
        } else {
            dLog("Internet Not Available")
            return false
        }
    }
    func removeAndAddObserver(str:String){
    	NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: str), object: nil)
    }
    
    
    //For String Validation
    func checkNullString(_ str:String!)->String {
        if let str1 = str {
            switch str1 {
            case "<null>","(null)","null","","0"," ":
                return ""
            case (_) where str1.isEmpty:
                return ""
            default:
                return str1
            }
        }
        return "";
    }
    //For Checking device is Ipad or not
    func isIpad() -> ObjCBool {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return true;
        }
        return false;
    }
    
    //General Alert Method
    func alert(_ title:String, message:String,vc:UIViewController)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: OK, style: UIAlertActionStyle.default, handler: { action in
            
        }))
        vc.present(alert, animated: true, completion: nil)
    }
    /*To get width of text - where height is fixed*/
    func getWidthOfText(_ height: CGFloat, font: UIFont,text:String) -> CGFloat{
        let constraintRect = CGSize(width:CGFloat.greatestFiniteMagnitude , height:height)
        let boundingBox = text.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.width
    }
    /*To get Height of text - where width is fixed*/
    func getHeightOfText(_ width: CGFloat, font: UIFont,text:String) -> CGFloat{
        let constraintRect = CGSize(width:width , height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.height
    }
    
    //For Checking System  OS version
    func isLatestVersion() -> ObjCBool{
        
        switch UIDevice.current.systemVersion.compare("8.0.0", options: NSString.CompareOptions.numeric) {
            
        case .orderedSame, .orderedDescending:
            
            dLog("iOS >= 8.0")
            return true
            
        case .orderedAscending:
            
            dLog("iOS < 8.0")
            return false
        }
    }
    //MARK:
    //MARK:Control Initilization Method
    //MARK:
    
    
    func configTextField(_ frame:CGRect, backCol:UIColor, textCol:UIColor,placeHolderText:String, placeHolderTextColor:UIColor, font:UIFont) -> UITextField{
        let textField = UITextField()
        textField.frame = frame
        textField.font = font
        textField.textColor = textCol
        textField.attributedPlaceholder = NSAttributedString(string:placeHolderText,
        attributes:[NSForegroundColorAttributeName: placeHolderTextColor])
        
        return textField
    }
    func configLabelFrame(_ frame:CGRect, backColor:UIColor, fontColor:UIColor, text:String, textAlign:NSTextAlignment,fontName:String , fontSize:CGFloat) -> UILabel{
        var text = text
        text = checkNullString(text)
        let lbl = UILabel()
        lbl.frame = frame
        lbl.backgroundColor = backColor
        lbl.textColor = fontColor
        lbl.textAlignment = textAlign
        lbl.text = text
        lbl.font = UIFont (name: fontName, size: fontSize)
        
        return lbl
    }
    
    func configView(_ frame:CGRect, bColor:UIColor) -> UIView{
        let view = UIView()
        view.frame = frame
        view.backgroundColor = bColor
        
        return view
    }
    
    
    func configImageView(_ frame:CGRect, imageName:String, backColor:UIColor, contentMode:UIViewContentMode) -> UIImageView{
        let imageView = UIImageView()
        imageView.frame = frame
        imageView.backgroundColor = backColor
        imageView.contentMode = contentMode
        imageView.image = UIImage(named: imageName)
        
        return imageView
    }
    func configTextView1(_ frame:CGRect , tColor:UIColor ,BColor:UIColor, scrollEnable:Bool , verticalScrolll:Bool , font:UIFont , editable:Bool ,text:String , textAlign : NSTextAlignment ) ->UITextView{
        let tView = UITextView (frame:frame)
        
        tView.backgroundColor = BColor
        tView.textColor   = tColor
        tView.isScrollEnabled = scrollEnable
        tView.showsVerticalScrollIndicator = verticalScrolll
        tView.font = font
        tView.isEditable = editable
        tView.textAlignment = textAlign
        tView.text = text
        
        tView.textAlignment = .left
        return tView
    }

    func configTextView(_ frame:CGRect , bColor:UIColor , tColor : UIColor , textAlign:NSTextAlignment , fontName : String , fontSize : CGFloat , content:String , isEditable:Bool) -> UITextView{
        let txtView = UITextView(frame: frame)
        txtView.backgroundColor = bColor
        txtView.textColor = tColor
        txtView.textAlignment = textAlign
        txtView.font = UIFont (name: fontName, size: fontSize)
        txtView.text = content
        var textH = self.getHeightOfText(txtView.frame.size.width, font: txtView.font!, text: txtView.text)
        var textW = self.getWidthOfText(textH, font: txtView.font!, text: txtView.text)
        if (textW >= txtView.frame.size.width - 20){
            textW  = txtView.frame.size.width - 10
            textH = self.getHeightOfText(txtView.frame.size.width, font: txtView.font!, text: txtView.text)
        }
        txtView.frame = CGRect(x: txtView.frame.origin.x, y: txtView.frame.origin.y, width: textW + 10, height: textH + 10)
        txtView.isEditable = isEditable
        txtView.isScrollEnabled = false
        return txtView
    }

    func configureScrollView(_ frame:CGRect, backColor:UIColor,contentWidth:CGFloat,contentHeight:CGFloat,isPaging:Bool)->UIScrollView{
        let scrollView = UIScrollView (frame:frame)
        scrollView.contentSize = CGSize(width: contentWidth,height: contentHeight)
        scrollView.isPagingEnabled = isPaging
        scrollView.backgroundColor   = backColor
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }
    
    
    func replaceSpaceWithDot(str: String) -> String{
        return str.replacingOccurrences(of: " ", with: ".")
    }

    //MARK:
    //MARK: Remove View Method
    //MARK:
    
    func removeViewUsindTag(_ viewTag:Int,inView:UIView){
        for view in inView.subviews{
            if (view.tag == viewTag){
                view.removeFromSuperview()
            }
        }
    }
    func removeAllViewFromView(_ inView:UIView){
        for view in inView.subviews{
            view.removeFromSuperview()
        }
    }
    func returnPriceText(price: Double) -> String{
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency
        currencyFormatter.currencyCode = "CHF"
        let priceString = currencyFormatter.string(from: price as NSNumber)
        return priceString!
    }

    //MARK:
    //MARK: Loading Method
    //MARK:
    
    
    func initiateLoaderWithMessage(inView:UIView){
        // var str = str
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        self.viewLoader = UIView(frame: CGRect(x: 0, y: 0, width:inView.frame.size.width, height: inView.frame.size.height))
        let imgBg = UIImageView(frame: self.viewLoader!.frame)
        imgBg.alpha = 0.5
        imgBg.backgroundColor = BC
        
        appDelegate.window?.addSubview( self.viewLoader!)
        self.viewLoader!.addSubview(imgBg)
        
        
        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "new_loader", withExtension: "gif")!)
        let advTimeGif = UIImage.gifImageWithData(imageData!)
        imgLoadingBG = UIImageView(image: advTimeGif)
        imgLoadingBG.frame = CGRect(x: 100.0, y: 220.0, width: inView.frame.size.width/2, height: 108.0)
        imgLoadingBG.center = inView.center
        imgLoadingBG.contentMode = .scaleAspectFit
        self.viewLoader?.addSubview(imgLoadingBG)
        
        
        /*let imgLoadingBG = UIImageView(frame: CGRect(x: self.viewLoader!.frame.width/2, y: self.viewLoader!.frame.height/2, width: 100, height: 100))
        
        imgLoadingBG.center = self.viewLoader!.center
        imgLoadingBG.alpha = 0.8
        imgLoadingBG.layer.cornerRadius = 10
        imgLoadingBG.backgroundColor = UIColor.black
        self.viewLoader!.addSubview(imgLoadingBG)*/
        
       /* let activityLoader = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        activityLoader.frame = CGRect(x: imgLoadingBG.frame.width/2, y: imgLoadingBG.frame.height/2, width: 50, height: 50)
        activityLoader.center = self.viewLoader!.center
        self.viewLoader!.addSubview(activityLoader)*/
        
      //  activityLoader.startAnimating()
        
    }
    func initiateLoaderWithMessageWithoutLoading(inView:UIView){
        
    }
    
    func HideLoader(){
        if let tempViewLoader = self.viewLoader{
            tempViewLoader.isHidden = true
            tempViewLoader.removeFromSuperview()
        }
    }
    
    
    
   
    
    
    //Error View Related Methods

    func generateAPIKEY() -> String
    {
        dLog("API Key = \(Date().timeIntervalSince1970 * 1000)")
        return "\(Date().timeIntervalSince1970 * 1000)"
    }
    
    //Pass LatLong and Get Image From Google
    
    func setLeftPadding(_ textField:UITextField , top : CGFloat , bottom : CGFloat , left : CGFloat , right : CGFloat ){
        let paddingView =  UIView (frame:CGRect(x: top,y: bottom,width: left,height: right))
        textField.leftView = paddingView
        textField.leftViewMode = .always
    }
    func shadowEffectInButton(_ vw:UIView){
        vw.layer.shadowColor = BC.cgColor
        vw.layer.shadowRadius = 4.0;
        vw.layer.shadowOpacity = 0.9;
        vw.layer.shadowOffset = CGSize.zero;
        vw.layer.masksToBounds = false;
    }

    /*Get Camera permission from user before using it with respective view controller*/
    func getCameraPermissionAndVC(_ vc:UIViewController , permissionDelegate:CameraPermission) -> ObjCBool {
        
        viewController = vc ;
        var isPermission:ObjCBool = false
        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if(authStatus == AVAuthorizationStatus.authorized){
            isPermission = true;
            permissionDelegate.performActionAfterCameraPermission(true)
        }
        else if(authStatus == AVAuthorizationStatus.denied){
            dLog("Camera access not determined. Ask for permission.")
           // permissionDelegate.performActionAfterCameraPermission(false)
            self.camDeniedFromVC(vc: vc)
        }
        else if(authStatus == AVAuthorizationStatus.notDetermined){
            dLog("Camera access not determined. Ask for permission")
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {
                granted in      // no need to specify the type, it is inferred from the completion block's signature
                if granted {    // the if condition does not require parens
                    
                    dLog("granted access to \(AVMediaTypeVideo)")
                    isPermission = true;
					permissionDelegate.performActionAfterCameraPermission(true)
                    
                } else {                    
                    dLog("Not granted access to \(AVMediaTypeVideo)")
                    permissionDelegate.performActionAfterCameraPermission(false)

                  //  self.camDeniedFromVC();
                }
            })
        }
        else if(authStatus == AVAuthorizationStatus.restricted)
        {
            self.alert("Error", message: "You've been restricted from using the camera on this device. Without camera access this feature won't work.", vc: vc)
            //alert2("Error", message: "You've been restricted from using the camera on this device. Without camera access this feature won't work.")
        }
        return isPermission
    }

    
    func getCameraPermission(_ vc:UIViewController) -> ObjCBool {
        
        viewController = vc ;
        var isPermission:ObjCBool = false
        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if(authStatus == AVAuthorizationStatus.authorized)
        {
            isPermission = true;
        }
        else if(authStatus == AVAuthorizationStatus.denied)
        {
            dLog("Camera access not determined. Ask for permission.")
            
            self.camDeniedFromVC(vc: vc);
        }
        else if(authStatus == AVAuthorizationStatus.notDetermined)
        {
            dLog("Camera access not determined. Ask for permission")
            
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {
                granted in      // no need to specify the type, it is inferred from the completion block's signature
                if granted {    // the if condition does not require parens
                    
                    dLog("granted access to \(AVMediaTypeVideo)")
                    isPermission = true;

                } else {
                    
                    dLog("Not granted access to \(AVMediaTypeVideo)")
                    self.camDeniedFromVC(vc: vc)
                }
            })
            
        }
        else if(authStatus == AVAuthorizationStatus.restricted)
        {
            self.alert("Error", message: "You've been restricted from using the camera on this device. Without camera access this feature won't work.", vc: vc)
           // alert2("Error", message: "You've been restricted from using the camera on this device. Without camera access this feature won't work.")
        }
        return isPermission
    }
    
    func showError(_ error: Error, vc: UIViewController)
    {
        self.alertErrorController("ALERT", message: error.localizedDescription, vc: vc)
    }
    
    func camDeniedFromVC(vc: UIViewController)
    {
        
    }
    
  /*  func camDeniedFromVC()
    {
        dLog("Denied camera access")
        
        var alertText = ""
        
        let alertButton = "OK"
        
            if(!UIApplicationOpenSettingsURLString.isEmpty)
            {
                alertText = "It looks like your privacy settings are preventing us from accessing your camera."
                
                alertText = "It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Touch Privacy.\n\n3. Turn the Camera on.\n\n4. Open this app and try again."
                
            }
            else
            {
                alertText = "It looks like your privacy settings are preventing us from accessing your camera.";
                
                alertText = "It looks like your privacy settings are preventing us from accessing your camera to do . You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
            }
        let alert = UIAlertView(title: "ALERT", message: alertText, delegate: self, cancelButtonTitle: alertButton)
        alert.tag = ERROR_TAG
        alert.show()
        } //else {
            // Fallback on earlier versions
        
//        let alert = UIAlertView(title: "ALERT", message: alertText, delegate: self, cancelButtonTitle: alertButton)
//        alert.tag = ERROR_TAG
//        alert.show()
    //}*/
    
    
    
    //MARK:
    //MARK: General Alert Method
    //MARK:
    
    
    
    
    func alertErrorController(_ title:String, message:String, vc: UIViewController)
    {
        let alertController1 = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction1 = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (action:UIAlertAction!) in
        }
        alertController1.addAction(OKAction1)
        vc.present(alertController1, animated: false, completion: nil)
    }
    
    
    
    func dismissAlert()
    {
        self.alertMsg.dismiss(animated: true, completion: nil)
    }
    /*To check weather app is runniung in simulator or device*/
    func checkAppInSimulator() ->Bool
    {
        var  DEVICE_IS_SIMULATOR : Bool!
        
        #if (arch(i386) || arch(x86_64)) && os(iOS)
            DEVICE_IS_SIMULATOR = true
            
        #else
            
            DEVICE_IS_SIMULATOR = false
        #endif
        
        return DEVICE_IS_SIMULATOR
    }
    
    func returnDate(_ date: Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        return dateFormatter.string(from: date.trimTime())
    }
    
    /*func generateRandomColor() -> UIColor
    {
        let hue : CGFloat = CGFloat(arc4random() % 256) / 256 // use 256 to get full range from 0.0 to 1.0
        let saturation : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from white
        let brightness : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from black
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 0.3)
    }*/
    
    
    
    //MARK:
    //MARK: Database Method
    //MARK:

    func loadImageUsingSDWebImage(url:String, imageView: UIImageView)
    {
        let url: URL? = URL(string: url)
        if let ur = url
        {
            imageView.sd_addActivityIndicator()
            imageView.sd_showActivityIndicatorView()
            imageView.sd_setIndicatorStyle(.gray)
            imageView.sd_setImage(with: ur) { (image, error, cache, urls) in
                if (error != nil) {
                } else {
                    // Successful in loading image
                }
            }
        }
    }
    
    func getCategoryString(txt: String, dict: NSDictionary) -> String
    {
        if let str = dict.value(forKey: txt) as? String
        {
            if(str == "")
            {
                return ""
            }
            else if(txt == "Category1")
            {
                return str
            }
            else
            {
                return "| " + str
            }
            
        }
        return ""
    }
    func checkNilValueForData1(_ key:String, dict: [String:Any])->Float{
        if let val = dict[key]{
            return Float(val as! Float)
        }
        return 0
    }
    
    func checkNilForFloat1(_ dic : [String:Any] , key:String)-> Float{
        var str = ""
        if let _ = dic[key] as? String{
            str = (dic[key] as? String)!
        }
        if Float(str)! == 0.0{
            return 1
        }
        return Float(str)!
    }
    
    func checkNilValueForFLoatValue1(_ key:String, dict: [String:Any])->Float{
        if let val = dict[key] as? Float{
            return Float(val )
        }
        return 0
    }
    
   
    
    //MARK:
    //MARK: NULL/nil Validation Method for different different purpose
    //MARK:
    
    func nanValidate(_ val:Float)->Float{
        var value = val
        if (val.isNaN){
            value = 0
        }else if (val <= 0){
            value = 0
        }
        return value
    }
    func checkNilValueForData(_ key:String, dict: NSMutableDictionary)->Float{
        if let val = dict.value(forKey: key){
            return Float(val as! Float)
        }
        return 0
    }
    func checkNilValueForFLoatValue(_ key:String, dict: NSMutableDictionary)->Float{
        if let val = dict.value(forKey: key) as? Float{
            return Float(val )
        }
        return 0
    }
    
    func checkNilForFloat(_ dic : NSDictionary , key:String)-> Float{
        var str = ""
        if let _ = dic.value(forKey: key) as? String{
            str = (dic.value(forKey: key) as? String)!
        }
        if Float(str)! == 0.0{
            return 1
        }
        return Float(str)!
    }
    func checkNilForString(_ dic : NSDictionary , key:String)-> Int{
        var str = ""
        if let _ = dic.value(forKey: key) as? String{
            str = (dic.value(forKey: key) as? String)!
        }
        return Int(str)!
    }
    func checkNilForStringReturnInt(_ dic : NSDictionary , key:String)-> Float{
        var  val : Float = 1.0
        if let _ = dic.value(forKey: key) as? NSNumber{
            val =  ((dic.value(forKey: key) as? NSNumber)?.floatValue)!
        }
        if val == 0{
            return 1
        }
        return val
    }
    func checkNilForStringReturnInt1(_ dic : [String:Any] , key:String)-> Float{
        var  val : Float = 1.0
        if let _ = dic[key] as? NSNumber{
            val =  ((dic[key] as? NSNumber)?.floatValue)!
        }
        if val == 0{
            return 1
        }
        return val
    }
    func checkNilForStringReturnFloat(_ dic : NSDictionary , key:String)-> Float{
        var  val : Float  = 1
        if let _ = dic.value(forKey: key) as? NSNumber{
            val =  Float((dic.value(forKey: key) as? NSNumber)!)
        }
        if val == 0{
            return 1
        }
        return val
    }
    func checkValueForFloatReturnInt(_ dic : NSDictionary , key:String)-> Int{
        var  val = 1
        if let _ = dic.value(forKey: key) as? Int{
            val =  (dic.value(forKey: key) as? Int)!
        }
        if val == 0{
            return 1
        }
        return val
    }
    func checkNilForStringReturnString(_ dic : NSDictionary , key:String)-> String
    {
        var str = ""
        if let _ = dic.value(forKey: key) as? String{
            str = (dic.value(forKey: key) as? String)!
        }
        return str
    }
    func checkNilForStringReturnString1(_ dic : [String:Any] , key:String)-> String{
        var str = ""
        if let _ = dic[key] as? String{
            str = (dic[key] as? String)!
        }
        return str
    }
    func checkNilValueFromDicForIntType(dict:NSDictionary , strKey:String)->Int{
        var val = 0
        if let _ = dict.value(forKey: strKey) as? Int{
            val = (dict.value(forKey: strKey) as? Int)!
        }
        return val
    }
    /*func checkNilValueFromDicForIntType1(dict:[String:Any] , strKey:String)->Int{
        var val = 0
        if let _ = dict[strKey] as? Int{
            val = (dict[strKey] as? Int!)!
        }
        return val
    }*/
    
    func updateProgresss(_ progView:UIProgressView,per:Float){
        progView.progress = per/100

    }
    func getHourMinFromDuration(duration: Int) -> (Int, Int){
        var min: Int!
        var sec: Int!
        if(duration != 0){
            if(duration % 60 == 0){
                min = duration/60
                sec = 0
            }else{
                min = Int(duration/60)
                sec = duration % 60
            }
        }else{
            min = 0
            sec = 0
        }
        return (min, sec)
        
    }
    func returnQuantityString(_ qnt:Float)->String{
        var q = qnt
        if qnt >= 1{
            q = round(qnt)
        }
        var str = String (format: "%.0f",q)
        if (qnt == 0.5){
            str = "1/2"
        }else if (qnt == 0.333333 || qnt == 0.33){
            str = "1/3"
        }else if (qnt == 0.25){
            str = "1/4"
        }else if (qnt == 0.0){
            str = "1"
        }
        return str
    }
    func getFoodQntAndStr(_ sender: UIStepper) ->(Float,String){
        
        var foodQnt = Float(sender.value)
        var q = foodQnt
        if foodQnt >= 1{
            q = round(foodQnt)
        }
        var str = String(format: "%.0f",q)
        if (sender.value == 0.0){
            foodQnt = 1/2
            str = "1/2"
        }else if (sender.value == -1){
            foodQnt = 1/3
            str = "1/3"
        }else if (sender.value == -2){
            foodQnt = 1/4
            str = "1/4"
        }
        return (foodQnt,str)
    }
    func setStepValue(_ sender:UIStepper , val:Float){
        if val == 0.5{
            sender.value = 0
        }else if val == 0.33  ||  val == 0.333333{
            sender.value = -1
        }else if val == 0.25 {
            sender.value = sender.minimumValue
        }else if val == 1{
        }
    }
    //environment
    
    func updateTopSummarybar(_ lbl:UILabel,per:Float,tempValue:Int,goal:Float){
        lbl.text = String(format: " %0.f %@ - %d / %.0f", per,"%",tempValue	,goal)
    }
    func updateLabel(_ lbl:UILabel, per:Float, str: String, startIn: Int, endIn: Int){
        if (per < 40){
           // lbl.textColor = UIColor(red: 229.0/255.0, green: 0.0/255.0, blue: 93.0/255.0, alpha: 1.0)
        }else if (per > 40 && per < 60 ){
           // lbl.textColor = UIColor(red: 241.0/255.0, green: 116.0/255.0, blue: 27.0/255.0, alpha: 1.0)
        }else{
           // lbl.textColor = UIColor(red: 51.0/255.0, green: 170.0/255.0, blue: 57.0/255.0, alpha: 1.0)
        }
        lbl.textColor = BC
    }  
    
    func updateLabel1(_ lbl:UILabel, per:Float, str: String, startIn: Int, endIn: Int){
        var lblColor = UIColor()
        if (per < 40){
            lblColor = UIColor(red: 229.0/255.0, green: 0.0/255.0, blue: 93.0/255.0, alpha: 1.0)
        }else if (per > 40 && per < 60 ){
             lblColor = UIColor(red: 241.0/255.0, green: 116.0/255.0, blue: 27.0/255.0, alpha: 1.0)
        }else{
             lblColor = UIColor(red: 51.0/255.0, green: 170.0/255.0, blue: 57.0/255.0, alpha: 1.0)
        }
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: str)
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: lblColor, range: NSRange(location:startIn,length:endIn))
        lbl.attributedText = myMutableString
    }
    
    //MARK:
    //MARK: Diary Calculation Method
    //MARK:
    
    //MARK:
    //MARK:Image Cache Method
    //MARK:
    func showCacheImage(_ imageURL: String, imageViews: UIImageView, delegate: AsynchImage? = .none){
        let imgUrl = URL(string: imageURL)
        if (self.cache.object(forKey: imgUrl?.lastPathComponent as AnyObject) != nil){
        	 imageViews.image = self.cache.object(forKey: imgUrl?.lastPathComponent as AnyObject) as? UIImage
        }
        else{
            let task = session.downloadTask(with: imgUrl!, completionHandler: { (location, response, error) -> Void in
                if let data = try? Data(contentsOf: imgUrl!){
                    DispatchQueue.main.async(execute: { () -> Void in
                        let img:UIImage! = UIImage(data: data)
                        imageViews.image = img
                         delegate?.imageDidLoad(image: img)
                        self.cache.setObject(img, forKey: imgUrl?.lastPathComponent as AnyObject)
                    })
                }
            })
            task.resume()
        }
    }
    func saveImageInCache(_ imageURL: String, imageViews: UIImageView, delegate: AsynchImage){
		let imgUrl = URL(string: imageURL)
        if (self.cache.object(forKey: imgUrl?.lastPathComponent as AnyObject) != nil){
        	 imageViews.image = self.cache.object(forKey: imgUrl?.lastPathComponent as AnyObject) as? UIImage
             delegate.imageDidLoad(image: imageViews.image!)
        }
        else{
            let task = session.downloadTask(with: imgUrl!, completionHandler: { (location, response, error) -> Void in
                if let data = try? Data(contentsOf: imgUrl!){
                    DispatchQueue.main.async(execute: { () -> Void in
                        let img:UIImage! = UIImage(data: data)
                        imageViews.image = img
                        delegate.imageDidLoad(image: img)
                        self.cache.setObject(img, forKey: imgUrl?.lastPathComponent as AnyObject)
                    })
                }
            })
            task.resume()
        }
    }
    func checkURLContentData(_ urlString:String,imageView:UIImageView){
        let imgUrl = URL(string: urlString)
       // imageView.image = UIImage(named: defImg)
        if (self.cache.object(forKey: imgUrl?.lastPathComponent as AnyObject) != nil){
            imageView.image = self.cache.object(forKey: imgUrl?.lastPathComponent as AnyObject) as? UIImage
        }
        else{
            let task = session.downloadTask(with: imgUrl!, completionHandler: { (location, response, error) -> Void in
                if let data = try? Data(contentsOf: imgUrl!)
                {
                    DispatchQueue.main.async(execute: { () -> Void in
                        
                        let img:UIImage! = UIImage(data: data)
                        imageView.image = img
                        self.cache.setObject(img, forKey: imgUrl?.lastPathComponent as AnyObject)
                    })
                }
            })
            task.resume()
        }
    }
    
    func hexStringToUIColor (_ hex:String) -> UIColor {
        
        var cString:String = hex.trimmingCharacters(in:CharacterSet.whitespacesAndNewlines)
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.index(cString.startIndex, offsetBy: 1))
        }
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
   
    func getStringFromDate()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        return  dateFormatter.string(from: Date().trimTime())
    }
    
    func returnWarningImage()->UIImageView{
        let imageView = UIImageView(image:#imageLiteral(resourceName: "picto-attention"))
        imageView.tintColor = .red
        constraintInfo(info: imageView)
        return imageView
    }
    
    func displayTagImage(tagId:Int)->UIImageView{
        if let image = UIImage(named: "tag_\(tagId)")  {
            let imageView = UIImageView(image: image)
            imageView.tintColor = NAV_BLUE_COLOR
            constraintInfo(info: imageView)
            return imageView
        }
        return UIImageView()
    }
    func addImage(image:UIImage)->UIImageView{
        let imageView = UIImageView(image: image)
        imageView.tintColor = .orange
        constraintInfo(info: imageView)
        return imageView
    }
    func setPictureView(allergenList:[[String: AnyObject]], infos: inout [UIView],img:UIImage,strkey:String , col :UIColor){
        if allergenList.filter( { $0["Name"] as? String == strkey } ).count > 0 {
            let imageView = UIImageView(image: img)
            imageView.tintColor = col
            constraintInfo(info: imageView)
            infos.append(imageView)
        }
    }
    func constraintInfo(info: UIImageView) {
        let squareConstraint = NSLayoutConstraint(
            item: info,
            attribute: .width,
            relatedBy: .equal,
            toItem: info,
            attribute: .height,
            multiplier: 1,
            constant: 0)
        info.addConstraint(squareConstraint)
    }
    
    func addRightMarkImageToLoading() {
        imgLoadingBG.image = UIImage(named: "like")
        imgLoadingBG.tintColor = DEFAULT_RED
    }

    func HideLoader1()  {
        if let tempViewLoader = self.viewLoader {
            tempViewLoader.isHidden = true
            tempViewLoader.removeFromSuperview()
        }
    }
}
extension Date {
    func trimTime() -> Date {
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let components = (cal as NSCalendar).components([.day, .month, .year], from: self)
        return cal.date(from: components)!
    }
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: Date())!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: Date())!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    var millisecondsSince1970:Double {
       let totalsSeconds = (self.timeIntervalSince1970 * 1000.0).rounded()
       /* guard !(Int((self.timeIntervalSince1970 * 1000.0).rounded())) else {
            return "illegal value" // or do some error handling
        }*/
        return totalsSeconds
        // Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
}
extension UIViewController {
    
    func returnName(fname:String,uName:String)->String{
        return "\(fname.replacingOccurrences(of: " ", with: ".").uppercased()).\(uName.replacingOccurrences(of: " ", with: ".").uppercased())"
    }
    
    /* func resolveHashTags(text: inout String) -> NSMutableAttributedString {
        let schemeMap = [
            "#":"hash",
            "@":"mention",
            "hash":"#",
            "mention":"@"
        ]
        
        // Separate the string into individual words.
        // Whitespace is used as the word boundary.
        // You might see word boundaries at special characters, like before a period.
        // But we need to be careful to retain the # or @ characters.
        let words = text.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        let attributedString = NSMutableAttributedString(string: text)
        
        // keep track of where we are as we interate through the string.
        // otherwise, a string like "#test #test" will only highlight the first one.
        var bookmark = text.startIndex
        
        // Iterate over each word.
        // So far each word will look like:
        // - I
        // - visited
        // - #123abc.go!
        // The last word is a hashtag of #123abc
        // Use the following hashtag rules:
        // - Include the hashtag # in the URL
        // - Only include alphanumeric characters.  Special chars and anything after are chopped off.
        // - Hashtags can start with numbers.  But the whole thing can't be a number (#123abc is ok, #123 is not)
        for word in words {
            
            var scheme:String? = nil
            
            if word.hasPrefix("#") {
                scheme = schemeMap["#"]
            } else if word.hasPrefix("@") {
                scheme = schemeMap["@"]
            }
            
            // Drop the # or @
            var wordWithTagRemoved = String(word.dropFirst())
            
            // Drop any trailing punctuation
            //wordWithTagRemoved.dropTrailingNonAlphaNumericCharacters()
            
            // Make sure we still have a valid word (i.e. not just '#' or '@' by itself, not #100)
            guard let schemeMatch = scheme, Int(wordWithTagRemoved) == nil && !wordWithTagRemoved.isEmpty
                else { continue }
            
            // URL syntax is http://123abc
            
            // Replace custom scheme with something like hash://123abc
            // URLs actually don't need the forward slashes, so it becomes hash:123abc
            // Custom scheme for @mentions looks like mention:123abc
            // As with any URL, the string will have a blue color and is clickable
            
            var target = wordWithTagRemoved
            var cleanedWord = "\(schemeMap[scheme!]!)\(wordWithTagRemoved)"
            if scheme == "mention" {
                let splittedWord = wordWithTagRemoved.components(separatedBy: "//")
                guard splittedWord.count == 2, let _ = Int(splittedWord[1])
                    else { continue }
                wordWithTagRemoved = splittedWord[0]
                cleanedWord = "\(schemeMap[scheme!]!)\(wordWithTagRemoved)"
                dLog(cleanedWord)
                text = text.stringByReplacingFirstOccurrenceOfString(target: word, withString: cleanedWord)
                attributedString.mutableString.replaceCharacters(in: attributedString.mutableString.range(of: word, options: .literal), with: cleanedWord)
                dLog(text)
                dLog(attributedString)
                if splittedWord.count > 1 {
                    target = splittedWord[1]
                } else {
                    target = splittedWord[0]
                }
            }
            let remainingRange = Range(bookmark..<text.endIndex)
            
            if let matchRange = text.range(of: cleanedWord, options: .literal, range: remainingRange),
                let escapedString = target.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                attributedString.addAttribute(NSLinkAttributeName, value: "\(schemeMatch):\(escapedString)", range: text.NSRangeFromRange(range: matchRange))
            }
            // just cycled through a word. Move the bookmark forward by the length of the word plus a space
            dLog(cleanedWord.count)
            bookmark = text.index(bookmark, offsetBy: cleanedWord.count - 1)
        }
        return attributedString
    }*/
  
    func resizeImage(image: UIImage) -> UIImage {
        let targetSize = CGSize(width: 1024, height: 1024)
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: image.size.width * heightRatio, height: image.size.height * heightRatio)
        } else {
            newSize = CGSize(width: image.size.width * widthRatio, height: image.size.height * widthRatio)
        }
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        CustomPhotoAlbum.sharedInstance.saveImage(image: newImage!)
        return newImage!
    }
    func checkValueFromDictionary(_ msgDict:[String:Any], key:String) -> String  {
        var str = ""
        if let val = msgDict[key] as? NSNumber   {
            str   = String (format: "%@",val)
        }
        return str
    }
    func getStringFromDic(dict:NSDictionary,foKey:String)->String{
        if let str = dict.value(forKey: foKey) as? String  {
            return  str
        }
        return ""
    }
    func getArrFromDic(dict:NSDictionary,foKey:String)->NSArray{
        if let arr = dict.value(forKey: foKey) as? NSArray {
            return  arr
        }
        return NSArray()
    }

    
    func startActivityIndicator(){
        Dispatch.DispatchQueue.main.async(execute: {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
     })
    }
    func hideLoading(){

        Dispatch.DispatchQueue.main.async(execute: {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            GlobalClass.sharedInstance.HideLoader()
        })
    }
}
extension String  {
        func isStringAnInt() -> Bool {
            if let _ = Int(self) {
                return true
            }
            return false
        }
}

    class CustomPhotoAlbum {

        static let albumName = APP_NAME
        static let sharedInstance = CustomPhotoAlbum()
        var assetCollection: PHAssetCollection!

        init() {

            func fetchAssetCollectionForAlbum() -> PHAssetCollection! {
                let fetchOptions = PHFetchOptions()
                fetchOptions.predicate = NSPredicate(format: "title = %@", CustomPhotoAlbum.albumName)
                let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
                if let _: AnyObject = collection.firstObject {
                    return collection.firstObject //as! PHAssetCollection
                }
                return nil
            }

            if let assetCollection = fetchAssetCollectionForAlbum() {
                self.assetCollection = assetCollection
                return
            }
            PHPhotoLibrary.shared().performChanges({
                PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: CustomPhotoAlbum.albumName)
            }) { success, _ in
                if success {
                    self.assetCollection = fetchAssetCollectionForAlbum()
                }
            }
        }
        func saveImage(image: UIImage) {

            if assetCollection == nil {
                return   // If there was an error upstream, skip the save.
            }
            PHPhotoLibrary.shared().performChanges({
                let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
                let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
                let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
                let fastEnumeration = NSArray(array: [assetPlaceholder] as! [PHObjectPlaceholder])
                albumChangeRequest?.addAssets(fastEnumeration)
                //albumChangeRequest?.addAssets([assetPlaceholder])
            }, completionHandler: nil)
        }
    }
