//
//  url.swift

//
//  Created by SYSMeta IT on 29/10/15.
//  Copyright © 2015 SYSMeta IT. All rights reserved.
//

import Foundation


let BASE_URL          = "http://qsrapi.azurewebsites.net"
                        


extension String {
    func stringByAddingPercentEncodingForRFC3986() -> String? {
        let unreserved = "-._~"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        return addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
    }
}
