//
//  CustomImagePicker.swift
//  Nutriligent
//
//  Created by Bharti Soft Tech Pvt Ltd on 31/01/18.
//  Copyright © 2018 SYSMeta IT. All rights reserved.
//

import UIKit

protocol ImagePickerDelegate {
    func returnPickerImage(_ img: UIImage)
}

class CustomImagePicker: UIImagePickerController, CameraPermission, UINavigationControllerDelegate, UIImagePickerControllerDelegate { // UICollectionViewDelegate
    var imagePickerDelegate: ImagePickerDelegate?
    var vc: UIViewController!
    var onlyCamera = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func initPicker() {
        _ = GlobalClass.sharedInstance.getCameraPermissionAndVC(self, permissionDelegate: self)
    }

    /*
     func initPicker(){
     #if (arch(i386) || arch(x86_64)) && os(iOS)
     GlobalClass.sharedInstance.alert(Alert, message: "This functionality is not applicable in simulator.", vc: vc)
     //_ = self.navigationController?.popViewController(animated: false)
     #else
     if(!Platform.isSimulator){
     let _ = GlobalClass.sharedInstance.getCameraPermissionAndVC(self,permissionDelegate: self)
     }
     #endif

     }
     */
    func openOnlyCamera() {
        #if (arch(i386) || arch(x86_64)) && os(iOS)
            GlobalClass.sharedInstance.alert(Alert, message: "This functionality is not applicable in simulator.", vc: vc)
            // _ = self.navigationController?.popViewController(animated: false)
        #else
            if !Platform.isSimulator {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera
                imagePicker.cameraCaptureMode = .photo
                imagePicker.cameraDevice = .rear // .front
                self.vc.present(imagePicker, animated: true, completion: nil)
            }
        #endif
    }

    func openCameraAndPhotoLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Image From Camera", comment: ""), style: .default, handler: { _ in
            #if (arch(i386) || arch(x86_64)) && os(iOS)
                GlobalClass.sharedInstance.alert(Alert, message: "This functionality is not applicable in simulator.", vc: self.vc)
                // _ = self.navigationController?.popViewController(animated: false)
            #else
                if !Platform.isSimulator {
                    imagePicker.sourceType = .camera
                    imagePicker.cameraCaptureMode = .photo
                    imagePicker.cameraDevice = .rear // .front
                    self.vc.present(imagePicker, animated: true, completion: nil)
                }
            #endif

        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Image From Library", comment: ""), style: .default, handler: { _ in
            imagePicker.sourceType = .photoLibrary
            self.vc.present(imagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: Cancel, style: UIAlertActionStyle.cancel, handler: { _ in
        }))
        self.vc.present(alert, animated: true, completion: {
            dLog("completion block")
        })
    }

    func performActionAfterCameraPermission(_ permission: Bool) {
        DispatchQueue.main.async(execute: {
            if permission {
                if self.onlyCamera {
                    self.openOnlyCamera()
                } else {
                    self.openCameraAndPhotoLibrary()
                }
            } else {
                GlobalClass.sharedInstance.camDeniedFromVC(vc: self)
            }
        })
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        var image: UIImage!
        if picker.sourceType == .camera {
            if (info[UIImagePickerControllerReferenceURL] as? URL) != nil {
                image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            }
          //  image = resizedImg
        } else {
            if (info[UIImagePickerControllerReferenceURL] as? URL) != nil {
                image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            }
        }
        let img = self.resizeImage(image: image)

        self.perform(#selector(self.dismissImagePicker), with: self, afterDelay: 0.1)
        if self.imagePickerDelegate != nil {
            self.imagePickerDelegate?.returnPickerImage(img)
        }
    }
    func dismissImagePicker() {
        if vc != nil {
            vc.dismiss(animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
