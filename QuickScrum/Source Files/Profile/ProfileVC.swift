//
//  ProfileVC.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 11/02/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
class ProfileVC: UIViewController , UITableViewDataSource , UITableViewDelegate , UITextFieldDelegate{

    @IBOutlet weak var tblProfile : UITableView!
    var user : User!
    override func viewDidLoad() {
        super.viewDidLoad()
        if tblProfile != nil {
            tblProfile.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCell")
            tblProfile.register(UINib(nibName: "ProfileImageView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ProfileImageView")
        }
    }
    @IBAction func cancelClicked(){
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }
  /*  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 44//UITableViewAutomaticDimension
    }*/
    
   /* func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Change Password"
    }*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50//UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
                return 150//
        }
        else {
            return 20
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ProfileImageView") as?ProfileView else {
                print("UserObject is nil")
                return UIView()
            }
            headerView.profileImage.image = #imageLiteral(resourceName: "profileimg_men")
            return headerView
        }
        return UIView()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          if let  cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as? ProfileCell,user != nil {
            if indexPath.section == 0 {
                cell.lblTitle.text = String(format:"Email : %@",user.loginEmailId!)
            }
            else if indexPath.section == 1 {
                cell.lblTitle.text = String(format:"Name : %@",user.fullName!)
            }
            else if indexPath.section == 2 {
                cell.lblTitle.text = String(format:"Short Name : %@",user.nameInitials!)
            }
            else if indexPath.section == 3 {
                cell.lblTitle.text = String(format:"Change Password")
            }
            return cell
        }    
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 3 {
            changePasswordClicked()
        }
    }
    
    func changePasswordClicked(){
        let alertController = UIAlertController(title: nil, message: NSLocalizedString("Change Password", comment: ""), preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default) { (action:UIAlertAction!) in
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: NSLocalizedString("Change", comment: ""), style: .default) { (action:UIAlertAction!) in
            guard let oldPass = alertController.textFields![0] as? UITextField else {return }
            guard let newPass = alertController.textFields![1] as? UITextField else {return }
            guard let confirmPass = alertController.textFields![2] as? UITextField else {return }
           
            if newPass.text == confirmPass.text {
                self.changePasswordAPICall(oldPass: oldPass.text!, newPass: newPass.text!)
            }
            else {
                self.showAlert(message: "Passwork does not matched.")
            }
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Old Password"
            self.setCommonPropertyOf(textField: textField)
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
           textField.placeholder = "New Password"
            self.setCommonPropertyOf(textField: textField)
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Confirm Password"
            self.setCommonPropertyOf(textField: textField)
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func setCommonPropertyOf(textField:UITextField){
        textField.isSecureTextEntry = true
        textField.delegate = self
    }
    
    func changePasswordAPICall(oldPass:String,newPass:String){
        if GlobalClass.sharedInstance.checkInternet().boolValue,user != nil{
            let parameters  = [
                "email": String(format:"%@",self.user.loginEmailId!),
                "password": String(format:"%@",newPass),
                "oldPassword":String(format:"%@",oldPass)
                ] as [String : Any]
            //print(parameters)
            self.startLoading()
            let str = "\(BASE_URL)/api/v1/ChangePassword"
            Alamofire.request(str, method: .post, parameters: parameters, encoding: JSONEncoding.init(options: .init(rawValue: 0))).responseJSON { (responseData) -> Void in
                   print(responseData.result.value)
                switch responseData.result {
                case .success:
                     self.stopLoading()
                    dLog("Validation Successful")
                     
                     if let msg = responseData.result.value as? String {
                        self.showAlert(message: msg)
                     }                    
                    else if let responceDic = responseData.result.value as? [String: Any] {
                        if let _ = responceDic["resultData"] as? String {
                            UserDefaults.standard.set(nil, forKey: TOKEN)
                            UserDefaults.standard.set(false, forKey: IS_LOGIN)
                            UserDefaults.standard.synchronize()
                        }
                        DispatchQueue.main.async(execute: {
                            self.performSegue(withIdentifier: "HomeVC", sender: nil)
                        })
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
        else {
            self.showAlert(message: CHECK_INTERNET)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
