//
//  ProfileView.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 08/02/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit

class ProfileView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var profileImage : UIImageView!
    //@IBOutlet weak var backView : UIView!
    
    
    let gradientLayer = CAGradientLayer()
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.backgroundView?.backgroundColor = OC
        let colorTop = THEME_COLOR//UIColor(red: 192.0 / 255.0, green: 38.0 / 255.0, blue: 42.0 / 255.0, alpha: 1.0).cgColor
        let colorBottom = BC//UIColor(red: 35.0 / 255.0, green: 2.0 / 255.0, blue: 2.0 / 255.0, alpha: 1.0).cgColor
        gradientLayer.frame =  rect//CGRect(x: 0, y: 0, width: self.bounds.width, height: 200)
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 0.15]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
       // self.backgroundView?.layer.insertSublayer(gradientLayer, at: 0)
       // self.layer.addSublayer(gradientLayer)
    }
}
