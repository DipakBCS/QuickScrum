//
//  ProfileCell.swift
//  QuickScrum
//
//  Created by Bharti Soft Tech Pvt Ltd on 11/02/18.
//  Copyright © 2018 Bharti Soft Tech Pvt Ltd. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
